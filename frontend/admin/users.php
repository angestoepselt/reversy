<?php
if (!isset($_SESSION)) {
    session_start();
}


if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}
?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy-Admin</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="admin.css" rel="stylesheet">


</head>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
                MENU
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                reversy-Admin
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../mainpage/index.php">reversy</a></li>
                <li><a href="../login-page/logout.php">Logout</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="container-fluid main-container">
    <div class="col-md-2 sidebar">
        <div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"></div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <div class="side-menu-container">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span>
                                    Dashboard</a></li>
                            <li><a href="users.php"><span class="glyphicon glyphicon-user"></span> Users</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-stats"></span> Stats</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>

            </div>
        </div>
    </div>
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                Neuen Nutzer registrieren:
            </div>
            <div class="panel-body">


                <?php

                //Admins können hier die Nutzerregistrierung einsehen.
                if ($_SESSION['admin'] == 1) {

                    require '../login-page/database.php';

                    //Sicherheitsabfrage ob ein Nutzer permanent gelöscht werden soll
                    if (isset($_GET['aktion']) and $_GET['aktion'] == 'sicherheitsabfrage' and $_SESSION['user_id'] != $_GET['id']) {
                        if (isset($_GET['id'])) {
                            $id_einlesen = (INT)$_GET['id'];
                            echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><b> Soll Benutzer ' . $id_einlesen . ' unwiderruflich gelöscht werden?</b></div><br>';
                            echo '<a href="?aktion=loeschen&id=' . $id_einlesen . '" class="btn btn-danger" role="button">endgültig löschen</a>';
                            echo '<b> oder </b>';
                            echo '<a href="users.php" class="btn btn-warning" role="button">zurück</a>';
                            echo '<hr>';
                            $_GET['aktion'] = 'anzeigen';
                        }
                    }

                    //Finale Löschung des Benutzers
                    if (isset($_GET['aktion']) and $_GET['aktion'] == 'loeschen') {
                        if (isset($_GET['id'])) {
                            $id = (INT)$_GET['id'];
                            if ($id > 0) {
                                $sql = "DELETE FROM users WHERE id=(:lid) LIMIT 1";
                                $stmt = $conn->prepare($sql);

                                $stmt->bindParam(':lid', $id);

                                if ($stmt->execute()) {


                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <strong>Benutzer <?php $id ?> wurde gelöscht!</strong>
                                    </div>

                                    <?php
                                }
                            }
                        }
                    }


                    //Neuen Benutzer aus Formular in die Datenbank eintragen (Passwort verschlüsselung mot BCRYPT)
                    $message = '';

                    if (!empty($_POST['email']) && !empty($_POST['password'])):

                        $pwd = password_hash($_POST['password'], PASSWORD_BCRYPT);

                        $sql = "INSERT INTO users (email, password) VALUES (:email, :password)";
                        $stmt = $conn->prepare($sql);

                        $stmt->bindParam(':email', $_POST['email']);
                        $stmt->bindParam(':password', $pwd);

                        if ($stmt->execute()):
                            $message = 'Successfully created new user';
                        else:
                            $message = 'Sorry there must have been an issue creating your account';
                        endif;

                    endif;


                    //Formular für die Erstellung von Benutzern
                    echo '<form action="users.php" method="POST">';

                    echo '<input type="text" placeholder="Email Adresse" name="email">';
                    echo '<input type="password" placeholder="Passwort" name="password">';
                    echo '<input type="password" placeholder="Passwort bestätigen" name="confirm_password">';
                    echo '<input type="submit">';

                    echo '</form>';
                    echo '</br>';
                    echo "<u>registrierte Nutzer:</u>";
                    echo '</br>';
                    echo '</br>';

                    //Tabelle Übersicht der aktuellen Benutzer aus DB
                    require '../login-page/database.php';
                    $sql = "SELECT * FROM users ORDER BY id";
                    echo '<table border="1">';
                    echo "<thead>";
                    echo "<th>" . "User-ID" . "</th>";
                    echo "<th>" . "Email" . "</th>";
                    echo "<th>" . "Admin" . "</th>";
                    echo "<th>" . "</th>";
                    echo "</thead>";

                    foreach ($conn->query($sql) as $row) {
                        echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['email'] . "</td>";
                        echo "<td>" . $row['admin'] . "</td>"; ?>
                        <td><a href="?aktion=sicherheitsabfrage&id=<?php echo $row['id']; ?>"> Benutzer löschen</a>
                        </td> <!-- Link zur Löschung verweist zuerst auf Sicherheitsabfrage -->
                        <?php
                        echo "</tr>";
                    }
                    echo "</table>";
                }

                ?>


            </div>
        </div>

</html>