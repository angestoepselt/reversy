<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/search.css" rel="stylesheet">


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
            <form class="navbar-form navbar-right">

            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a></li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li class="active"><a href="">Key-DB</a></li>

            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h3 class="sub-header">Key-DB</h3>
            <form class="form-horizontal" action="" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Schlüssel eintragen:</legend>
                    <div class="container">
                    <div class="row">
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col control-label" for="name"></label>
                        <div class="col-md-4">
                            <select id="name" name="name" class="form-control">
                                <option value="Bitte Software auswählen:" selected disabled>Bitte Software auswählen:</option>
                                <option value="WIN 7 Pro">WIN 7 Pro</option>
                                <option value="WIN 7 Home Premium">WIN 7 Home Premium</option>
                                <option value="WIN 7 Ultimate">WIN 7 Ultimate</option>
                                <option value="WIN 8 Pro">WIN 8 Pro</option>
                                <option value="WIN 8 Home Premium">WIN 8 Home Premium</option>
                                <option value="WIN 8 Ultimate">WIN 8 Ultimate</option>
                                <option value="WIN 10 Pro">WIN 10 Pro</option>
                                <option value="WIN 10 Home Premium">WIN 10 Home Premium</option>
                                <option value="WIN 10 Ultimate">WIN 10 Ultimate</option>
                                <option value="WIN Vista Pro">WIN Vista Pro</option>
                                <option value="WIN Vista Home">WIN Vista Home</option>
                                <option value="WIN Vista Business">WIN Vista Business</option>
                                <option value="WIN Vista Ultimate">WIN Vista Ultimate</option>
                                <option value="WIN XP Home">WIN XP Home</option>
                                <option value="WIN XP Pro">WIN XP Pro</option>
                                <option value="MS Office 2003 Pro">MS Office 2003 Pro</option>
                                <option value="MS Office 2003 Home &amp; Student">MS Office 2003 Home &amp; Student</option>
                                <option value="MS Office 2007 Pro">MS Office 2007 Pro</option>
                                <option value="MS Office 2007 Home &amp; Student">MS Office 2007 Home &amp; Student</option>
                                <option value="MS Office 2012 Pro">MS Office 2012 Pro</option>
                                <option value="MS Office 2012 Home &amp; Student">MS Office 2012 Home &amp; Student</option>
                                <option value="MS Office 2016 Pro">MS Office 2016 Pro</option>
                                <option value="MS Office 2016 Home &amp; Student">MS Office 2016 Home &amp; Student</option>
                                <option value="sonstiges">Sonstiges (bitte bei Key dazuschreiben)</option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col control-label" for="schluessel"></label>
                        <div class="col-md-5">
                            <input id="schluessel" name="schluessel" placeholder="Key" class="form-control input-md" required="" type="text">
                            <input type="hidden" name="aktion" value="insert">
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col control-label" for="submit"></label>
                        <div class="col-md-4">
                            <button id="submit" type="submit" name="submit" class="btn btn-primary" formmethod="post" formaction="">Key eintragen</button>
                        </div>
                    </div>
                    </div>
                    </div>

                </fieldset>
            </form>
            <hr>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


            <h3 class="sub-header">Key Übersicht</h3>


            <form class="form-horizontal">
                <fieldset>
                    <!-- Suche nach bestimmten Status in der Tabelle-->
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="selectstatus">Key Suche:</label>
                        <div class="col-md-2">
                            <select id="selectstatus" name="selectstatus" class="form-control">
                                <option value="Bitte Software auswählen:" selected disabled>Bitte Software auswählen:</option>
                                <option value="WIN 7 Pro">WIN 7 Pro</option>
                                <option value="WIN 7 Home Premium">WIN 7 Home Premium</option>
                                <option value="WIN 7 Ultimate">WIN 7 Ultimate</option>
                                <option value="WIN 8 Pro">WIN 8 Pro</option>
                                <option value="WIN 8 Home Premium">WIN 8 Home Premium</option>
                                <option value="WIN 8 Ultimate">WIN 8 Ultimate</option>
                                <option value="WIN 10 Pro">WIN 10 Pro</option>
                                <option value="WIN 10 Home Premium">WIN 10 Home Premium</option>
                                <option value="WIN 10 Ultimate">WIN 10 Ultimate</option>
                                <option value="WIN Vista Pro">WIN Vista Pro</option>
                                <option value="WIN Vista Home">WIN Vista Home</option>
                                <option value="WIN Vista Business">WIN Vista Business</option>
                                <option value="WIN Vista Ultimate">WIN Vista Ultimate</option>
                                <option value="WIN XP Home">WIN XP Home</option>
                                <option value="WIN XP Pro">WIN XP Pro</option>
                                <option value="MS Office 2003 Pro">MS Office 2003 Pro</option>
                                <option value="MS Office 2003 Home &amp; Student">MS Office 2003 Home &amp; Student</option>
                                <option value="MS Office 2007 Pro">MS Office 2007 Pro</option>
                                <option value="MS Office 2007 Home &amp; Student">MS Office 2007 Home &amp; Student</option>
                                <option value="MS Office 2012 Pro">MS Office 2012 Pro</option>
                                <option value="MS Office 2012 Home &amp; Student">MS Office 2012 Home &amp; Student</option>
                                <option value="MS Office 2016 Pro">MS Office 2016 Pro</option>
                                <option value="MS Office 2016 Home &amp; Student">MS Office 2016 Home &amp; Student</option>
                                <option value="sonstiges">Sonstiges (bitte bei Key dazuschreiben)</option>
                            </select>
                            <input type="hidden" name="aktion" value="statusSelect">
                        </div>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </fieldset>
            </form>
            <hr>



            <!-- Datenbank Tabellenkopf der Ausgabetabelle:-->
            <script src="../js/jquery.min.js"></script>
            <script src="../js/bootstrap.js/bootstrap.min.js"></script>

            <div class="container-fluid">
                <div class="row">


                    <div class="col-md-12">
                        <div class="table-responsive">


                            <table id="mytable" class="table table-bordred table-striped table-hover">

                                <thead>
                                <th>ID</th>
                                <th>Name/Typ</th>
                                <th>Key</th>
                                <th>Key löschen</th>
                                </thead>


                                <?php

                                require_once('../rechner_db/inc/db.php');


                                if (isset($_GET['aktion']) and $_GET['aktion'] == 'sicherheitsabfrage') {
                                    if (isset($_GET['id'])) {
                                        $id_einlesen = (INT)$_GET['id'];
                                        echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><b> Soll Datensatz ' . $id_einlesen . ' unwiderruflich gelöscht werden?</b></div><br>';
                                        echo '<a href="?aktion=loeschen&id=' . $id_einlesen . '" class="btn btn-danger" role="button">endgültig löschen</a>';
                                        echo '<b> oder </b>';
                                        echo '<a href="index.php" class="btn btn-warning" role="button">zurück</a>';
                                        echo '<hr>';
                                        $_GET['aktion'] = 'anzeigen';
                                    }
                                }

                                //Löschen eines Rechner-Datensatzes
                                if (isset($_GET['aktion']) and $_GET['aktion'] == 'loeschen') {
                                    if (isset($_GET['id'])) {
                                        $id = (INT)$_GET['id'];
                                        if ($id > 0) {
                                            $loeschen = $db->prepare("DELETE FROM keydb WHERE id=(?) LIMIT 1");
                                            $loeschen->bind_param('i', $id);
                                            if ($loeschen->execute()) {

                                                ?>
                                                <div class="alert alert-success alert-dismissable">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>Datensatz <?php $id ?> wurde gelöscht!</strong>
                                                </div>

                                                <?php
                                            }
                                        }
                                    }
                                }



                                if(isset($_POST['aktion']) and $_POST['aktion'] == 'insert'){

                                    $name = (isset($_POST["name"])) ? htmlspecialchars($_POST["name"]) : "";
                                    $schluessel = (isset($_POST["schluessel"])) ? htmlspecialchars($_POST["schluessel"]) : "";

                                    require('../rechner_db/inc/db.php');

                                    $sql = "INSERT INTO keydb (name, schluessel) VALUES (?, ?)";
                                    $kommando = mysqli_prepare($db, $sql);
                                    mysqli_stmt_bind_param($kommando, "ss", $name, $schluessel);
                                    if (mysqli_stmt_execute($kommando)) {

                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Key wurde erfolgreich engetragen!</strong>
                                        </div>
                                        <?php

                                    }
                                }




                                if (isset($_GET['anzeigen'])) {
                                        $anzeigen = $_GET['anzeigen'];
                                    } else {
                                        $anzeigen = "n";
                                    }

                                    //Ausgabe der Daten als Tabelle
                                    //Zusätzlich Unterscheidung wie viele Datensätze aus der Tabelle geladen werden, dazu Link am Tabellenende.
                                    if ($anzeigen === 'j') {

                                        $erg = $db->query("SELECT id, name, schluessel FROM keydb ORDER BY id DESC")
                                        or die($db->error);

                                    } else {

                                        //Hier würd überprüft, ob nach einer bestimmten Sorte von Keys sortiert werden soll
                                        if (isset($_GET['aktion'])) {
                                            $aktionStatus = $_GET['aktion'];
                                        }
                                        //Auswahl der Keys mit selektiertem Status
                                        if (isset($aktionStatus) && $aktionStatus === 'statusSelect') {
                                            if (isset($_GET['selectstatus'])) {

                                                $selectstatus = $_GET['selectstatus'];

                                                $erg = $db->query("SELECT id, name, schluessel FROM keydb WHERE name = '$selectstatus' ORDER BY id DESC")
                                                or die($db->error);
                                            }
                                        } else {

                                            $erg = $db->query("SELECT id, name, schluessel FROM keydb ORDER BY id DESC LIMIT 40")
                                            or die($db->error);
                                        }
                                    }
                                    // while ($zeile = $erg->fetch_assoc()) {
                                    while ($zeile = $erg->fetch_object()) {
                                        ?>

                                        <tr>
                                            <td>
                                                <?php echo $zeile->id; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->name; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->schluessel; ?>
                                            </td>
                                            <td><a href="?aktion=sicherheitsabfrage&id=<?php echo $zeile->id; ?>"
                                                   class='btn btn-danger btn-xs' data-title='Delete'><span
                                                            class='glyphicon glyphicon-trash'></span></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    echo "</table>";

                                    $erg->free();
                                    $db->close();

                                    //Bestimmt ob alle Datensätze aus dieser Tabelle geladen werden sollen.
                                    if (!isset($_GET['anzeigen'])) {
                                        echo "<a href='?anzeigen=j'>Alle Datensätze anzeigen</a>";
                                    }
                                    ?>

                                    </br>

                                </div>
                        </div>
                    </div>
                </div>

</div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>
</body>

</html>