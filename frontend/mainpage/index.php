<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

require_once('../rechner_db/inc/db.php');

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/counter.css" rel="stylesheet">

    <script src="../js/jquery-3.2.0.js"></script>


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy v2.0</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li>
                    <a href="../login-page/user.php"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>

                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Übersicht <span class="sr-only">(current)</span></a></li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li><a href="../pdf_docs/delete_confirm_form.php">Rechner Löschbestätigung</a></li>
                <li><a href="../pdf_docs/auslagen_form.php">Auslagen Erstattung</a></li>
                <?php //Admins können hier den Key_DB link benutzen.
                if ($_SESSION['admin'] == 1) { ?>
                <li><a href="../key_db/index.php">Key-DB</a></li>
                <?php
                }
                ?>
                <!--<li><a href="../mail/mail.php">Status Mailer</a></li>-->
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <!-- Javascript counter welcher aktuelle Zahlen rund um die Computerspende anzeigt. -->
            <div class="counter">

                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="employees">
                                <p class="counter-count">
                                    <?php
                                    $temp = $db->query("SELECT COUNT(*) as anzahl FROM rechner") or die($mysqli->error);
                                    $z = $temp->fetch_array();
                                    $anzahl = $z["anzahl"];
                                    echo $anzahl;
                                    ?>
                                </p>
                                <p class="employee-p"><a href="../rechner_db/index.php">Rechner</a></p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="customer">
                                <p class="counter-count">
                                    <?php
                                    $temp = $db->query("SELECT COUNT(*) as anzahl FROM antrag") or die($mysqli->error);
                                    $z = $temp->fetch_array();
                                    $anzahl = $z["anzahl"];
                                    echo $anzahl;
                                    ?></p>
                                <p class="customer-p"><a href="../antrag_db/index.php">Kunden</a></p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="design">
                                <p class="counter-count">697</p>
                                <p class="design-p"><a
                                            href="https://de-de.facebook.com/computerspende.wuerzburg/">Likes</a></p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="order">
                                <p class="counter-count">45</p>
                                <p class="order-p"><a href="#">Mitglieder</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Skript -->
            <script>
                $(document).ready(function () {
                    $('.counter-count').each(function () {
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 5000,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
                    $('.counter-count').mouseenter(function () {
                        $(this).css("background-color", "#ffffff");

                        $(this).css("color", "#428bca");
                    });
                    $('.counter-count').mouseleave(function () {
                        $(this).css("background-color", "#428bca");
                        $(this).css("color", "#ffffff");
                    });
                });
            </script>
            <hr>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>
</body>

</html>