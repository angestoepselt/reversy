<?php
// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');
//Das PDF wird durch TCPDF erstellt und zum Druck vorbereitet.

$datum = date("d.m.Y");

$name = (isset($_POST["name"])) ? htmlspecialchars($_POST["name"]) : "";
$vorname = (isset($_POST["vorname"])) ? htmlspecialchars($_POST["vorname"]) : "";
$strasse = (isset($_POST["strasse"])) ? htmlspecialchars($_POST["strasse"]) : "";
$ort = (isset($_POST["ort"])) ? htmlspecialchars($_POST["ort"]) : "";

$auslage1Datum = (isset($_POST["auslage1Datum"])) ? htmlspecialchars($_POST["auslage1Datum"]) : "";
$auslage1Leistender = (isset($_POST["auslage1Leistender"])) ? htmlspecialchars($_POST["auslage1Leistender"]) : "";
$auslage1Leistungsbeschreibung = (isset($_POST["auslage1Leistungsbeschreibung"])) ? htmlspecialchars($_POST["auslage1Leistungsbeschreibung"]) : "";
$auslage1Betrag = (isset($_POST["auslage1Betrag"])) ? htmlspecialchars($_POST["auslage1Betrag"]) : "";

$auslage2Datum = (isset($_POST["auslage2Datum"])) ? htmlspecialchars($_POST["auslage2Datum"]) : "";
$auslage2Leistender = (isset($_POST["auslage2Leistender"])) ? htmlspecialchars($_POST["auslage2Leistender"]) : "";
$auslage2Leistungsbeschreibung = (isset($_POST["auslage2Leistungsbeschreibung"])) ? htmlspecialchars($_POST["auslage2Leistungsbeschreibung"]) : "";
$auslage2Betrag = (isset($_POST["auslage2Betrag"])) ? htmlspecialchars($_POST["auslage2Betrag"]) : "";

$iban = (isset($_POST["iban"])) ? htmlspecialchars($_POST["iban"]) : "";
$bic = (isset($_POST["bic"])) ? htmlspecialchars($_POST["bic"]) : "";
$institut = (isset($_POST["institut"])) ? htmlspecialchars($_POST["institut"]) : "";

if ($auslage2Datum != "") {
    $gesamtAuszahlung = $auslage1Betrag + $auslage2Betrag;
} else {
    $gesamtAuszahlung = $auslage1Betrag;
}


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{


    public function Header()
    {
        // Set font
        $this->SetFont('helvetica', '', 14);
        // Title
        $this->writeHTMLCell(0, 0, '', '', $html =

            '<p>Angestöpselt e.V. - Verein für Digitalkompetenz<br>Computerspende Würzburg<br><hr></p>', 0, 1, 0, true, '', true);
    }

    // Page footerS
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-35);
        // Set font
        $this->SetFont('helvetica', '', 9);

        $this->writeHTMLCell(0, 0, '', '', $html =

            '<hr><p style="text-align: center;"><font size="6">Vorsitzende der Vereins Angestöpselt e.V. Steffen Hock, Moritz Beck, Florian Helmerich</font><br>
				Anschrift: <b>Frankfurter Str. 74, 97082 Würzburg</b> / Telefon: <b>0931-260 49 822</b><br>
				E-Mail: <b>info@angestoepselt.de</b> / Internet: <b>http://www.angestoepselt.de</b> / Facebook: <b>http://facebook.angestoepselt.de</b><br>
				Spendenkonto: angestöpselt e.V. / <b>Sparkasse Mainfranken Würzburg</b> / IBAN <b>DE23 7905 0000 0047 3098 28</b></p>
				
				', 0, 1, 0, true, '', true);
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Angestöpselt e.V.');
$pdf->SetTitle('Erstattung von Auslagen');
$pdf->SetSubject('Erstattung von Auslagen');


// remove default header/footer
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);


// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 12);

// add a page
$pdf->AddPage('P', 'A4');

// Set some content to print
$html = <<<EOD
<style>
h2 {
    text-align: right;
} 
</style>
<p>	<br>
		<p></p>
		<h2>Erstattung von Auslagen</h2>
		<br>
	Hiermit beantrage ich,<br>
	<br>
	$vorname $name<br>
	$strasse<br>
	$ort<br>
	<br>
	Die Erstattung der folgenden Auslagen. Alle vorhandenen Belege habe ich beigelegt.<br>
	
	<br>
	<br>
	
	
    
EOD;

if ($auslage2Datum == "") {
// Print text using writeHTMLCell() WENN nur eine Auslage vorhanden
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// ---------------------------------------------------------

    $tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
    	<td></td>
        <td>Datum</td>
        <td>Leistender</td>
        <td>Beschreibung</td>
        <td>Betrag</td>
    </tr>
    <tr>
    	<td>Auslage 1</td>
        <td>$auslage1Datum</td>
        <td>$auslage1Leistender</td>
        <td>$auslage1Leistungsbeschreibung</td>
        <td>$auslage1Betrag €</td>
    </tr>
    <tr>
       <td>GESAMMT</td>
       <td></td>
       <td></td>
       <td></td>
       <td>$gesamtAuszahlung €</td>
    </tr>

</table>
EOD;

    $pdf->writeHTML($tbl, true, false, false, false, '');
} else {
// Print text using writeHTMLCell() WENN 2 Auslagen vorhanden
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// ---------------------------------------------------------

    $tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
    	<td></td>
        <td>Datum</td>
        <td>Leistender</td>
        <td>Beschreibung</td>
        <td>Betrag</td>
    </tr>
    <tr>
    	<td>Auslage 1</td>
        <td>$auslage1Datum</td>
        <td>$auslage1Leistender</td>
        <td>$auslage1Leistungsbeschreibung</td>
        <td>$auslage1Betrag €</td>
    </tr>
    <tr>
       <td>Auslage 2</td>
       <td>$auslage2Datum</td>
       <td>$auslage2Leistender</td>
       <td>$auslage2Leistungsbeschreibung</td>
       <td>$auslage2Betrag €</td>
    </tr>
    <tr>
       <td>GESAMMT</td>
       <td></td>
       <td></td>
       <td></td>
       <td>$gesamtAuszahlung €</td>
    </tr>

</table>
EOD;

    $pdf->writeHTML($tbl, true, false, false, false, '');
}

$html2 = <<<EOD
<style>
sig1 {
    text-align: right;
} 
</style>
Ich bitte um Auszahlung auf das folgende Konto:<br>
<br>
$vorname $name<br>
$iban<br>
$bic<br>
$institut<br>
<br>
<br>
<p></p>
<br>
---------------------------------------------<br>
Ort, Datum $vorname $name<br>
<br>
<br>
<p></p>
<br>
-----------------------------------------------------------------------------------------<br>
Datum, Unterschritft(en) Vorstand<br>


    
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);
//Close and output PDF document
$pdf->Output('Antrag_Erstattung.pdf', 'D');
?>
