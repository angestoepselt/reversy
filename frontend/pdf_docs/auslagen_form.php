<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li>
                    <a href="../login-page/user.php"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="#">Hilfe</a></li>
                <li><a href="#">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a>
                </li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Auslagen Erstattungsantrag</a></li>
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <form class="form-horizontal" action="auslagen_pdf.php" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Auslagen Erstattung:</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Name:</label>
                        <div class="col-md-4">
                            <input id="name" name="name" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="vorname">Vorname:</label>
                        <div class="col-md-4">
                            <input id="vorname" name="vorname" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="strasse">Strasse:</label>
                        <div class="col-md-5">
                            <input id="strasse" name="strasse" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="ort">PLZ, Ort:</label>
                        <div class="col-md-5">
                            <input id="ort" name="ort" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>
                    </br>
                    <hr>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage1Datum">Datum Auslage1:</label>
                        <div class="col-md-4">
                            <input id="auslage1Datum" name="auslage1Datum" placeholder="zb. 01.01.2000"
                                   class="form-control input-md" required="" type="text">
                            <span class="help-block">Bitte gebe hier das Datum der Auslage an </span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage1Leistender">Leistender Auslage1:</label>
                        <div class="col-md-4">
                            <input id="auslage1Leistender" name="auslage1Leistender" placeholder="zb. Max Mustermann"
                                   class="form-control input-md" required="" type="text">

                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage1Leistungsbeschreibung">Beschreibung
                            Auslage1:</label>
                        <div class="col-md-4">
                            <input id="auslage1Leistungsbeschreibung" name="auslage1Leistungsbeschreibung"
                                   placeholder="zb. Bürobedarf, Werkzeug" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Appended Input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage1Betrag">Betrag Auslage1:</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input id="auslage1Betrag" name="auslage1Betrag" class="form-control"
                                       placeholder="zb. 10 oder 9.90" required="" type="text">
                                <span class="input-group-addon">€</span>
                            </div>
                            <p class="help-block">Bitte hier den Betrag ohne € Zeichen eintragen</p>
                        </div>
                    </div>
                    </br></br>

                    <h5><b>Falls vorhanden, Auslage2:</b></h5>
                    </br>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage1Datum">Datum Auslage2:</label>
                        <div class="col-md-4">
                            <input id="auslage2Datum" name="auslage2Datum" placeholder="zb. 01.01.2000"
                                   class="form-control input-md" type="text">
                            <span class="help-block">Bitte gebe hier das Datum der Auslage an </span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage2Leistender">Leistender Auslage2:</label>
                        <div class="col-md-4">
                            <input id="auslage2Leistender" name="auslage2Leistender" placeholder="zb. Max Mustermann"
                                   class="form-control input-md" type="text">

                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage2Leistungsbeschreibung">Beschreibung
                            Auslage2:</label>
                        <div class="col-md-4">
                            <input id="auslage2Leistungsbeschreibung" name="auslage2Leistungsbeschreibung"
                                   placeholder="zb. Bürobedarf, Werkzeug" class="form-control input-md" type="text">

                        </div>
                    </div>

                    <!-- Appended Input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auslage2Betrag">Betrag Auslage2:</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input id="auslage2Betrag" name="auslage2Betrag" class="form-control"
                                       placeholder="zb. 10 oder 9.90" type="text">
                                <span class="input-group-addon">€</span>
                            </div>
                            <p class="help-block">Bitte hier den Betrag ohne € Zeichen eintragen</p>
                        </div>
                    </div>
                    </br>

                    <!-- Multiple Radios (inline) -->

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="iban">IBAN:</label>
                        <div class="col-md-4">
                            <input id="iban" name="iban" placeholder="DE......" class="form-control input-md"
                                   required="" type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="bic">BIC:</label>
                        <div class="col-md-4">
                            <input id="bic" name="bic" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="institut">Institut:</label>
                        <div class="col-md-4">
                            <input id="institut" name="institut" placeholder="" class="form-control input-md"
                                   required="" type="text">

                        </div>
                    </div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="submit"></label>
                        <div class="col-md-4">
                            <button id="Reset" type="reset" name="Reset" class="btn btn-danger">Eingaben löschen
                            </button>
                            <button type="submit" class="btn btn-primary">erstellen</button>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>

</body>

</html>