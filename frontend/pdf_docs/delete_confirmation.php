<?php
// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');
//Das PDF wird durch TCPDF erstellt und zum Druck vorbereitet.

$datum = date("d.m.Y");
$vorname = $_POST["vorname"];
$nachname = $_POST["nachname"];
$firma = $_POST["firma"];
$strasse = $_POST["strasse"];
$ort = $_POST["ort"];
$vorstandname = $_POST["vorstandname"];
$geschlecht = $_POST["select1"];

if ($geschlecht == "m") {
    $anrede = "Sehr geehrter Herr $nachname,";
} else {
    $anrede = "Sehr geehrte Frau $nachname,";
}


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{


    public function Header()
    {
        // Set font
        $this->SetFont('times', '', 14);
        // Title
        $this->writeHTMLCell(0, 0, '', '', $html =

            '<p>Angestöpselt e.V. - Verein für Digitalkompetenz<br>Computerspende Würzburg<br><hr></p>', 0, 1, 0, true, '', true);
    }

    // Page footerS
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-35);
        // Set font
        $this->SetFont('times', '', 9);

        $this->writeHTMLCell(0, 0, '', '', $html =

            '<hr><p style="text-align: center;"><font size="6">Vorsitzende der Vereins Angestöpselt e.V. Steffen Hock, Moritz Beck, Florian Helmerich</font><br>
				Anschrift: <b>Frankfurter Str. 74, 97082 Würzburg</b> / Telefon: <b>0931-260 49 822</b><br>
				E-Mail: <b>info@angestoepselt.de</b> / Internet: <b>http://www.angestoepselt.de</b> / Facebook: <b>http://facebook.angestoepselt.de</b><br>
				Spendenkonto: angestöpselt e.V. / <b>Sparkasse Mainfranken Würzburg</b> / IBAN <b>DE23 7905 0000 0047 3098 28</b></p>
				
				', 0, 1, 0, true, '', true);
    }
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Angestöpselt e.V.');
$pdf->SetTitle('Löschbestätigung');
$pdf->SetSubject('Löschbestätigung V2');


// remove default header/footer
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);


// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 11);

// add a page
$pdf->AddPage('P', 'A4');

// Set some content to print
$html = <<<EOD
<style>
h5 {
    text-align: right;
} 
</style>
<p>	<br>
		<br>
		<p></p>
		<br>
	 $vorname $nachname<br>
	 $firma<br>
	 $strasse<br>
	 $ort</p>
	 </br>
	 </br>
<h5>Würzburg, $datum</h5>
</br>
<h3><b>Bestätigung der Löschung gespendeter Datenträger</b></h3>
</br>
<p>$anrede</p>
<br>

<p>hiermit bestätigen wir Ihnen, dass wir alle an uns gespendete Datenträger vor der Analyse durch unsere
Mitglieder und insbesondere vor der Weitergabe an unsere Kunden vollständig löschen. Um sicherzustellen, dass
die Daten auch mit handelsüblicher Software nicht mehr wiederherzustellen sind, benutzen wir das Werkzeug
DBAN (http://www.dban.org). Diese Software überschreibt die komplette Festplatte mehrmals, sodass alle Daten
unwiderruflich gelöscht werden.</p>
</br>
</br>

<p>Wir danken Ihnen für Ihr Vertrauen und für Ihre Spende.</p>
<p>Mit freundlichen Grüßen</p>
</br>
</br>
<p></p>
</br>
</br>
<p></p>
</br>
</br>
<font size="9"><p>$vorstandname</p>
<p>Vorstand Angestöpselt e.V.</p></font>

EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('loeschbestaetigung.pdf', 'D');
?>
