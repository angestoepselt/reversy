<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li>
                    <a href="../login-page/user.php"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="#">Hilfe</a></li>
                <li><a href="#">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a>
                </li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li class="active"><a href="../rechner_db/index.php">Rechner Löschbestätigung</a></li>
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- Formular zur automatischen Erstellung von Löschbestätigungen, welche im Anschluss gedruckt werden können -->
            <form class="form-horizontal" action="delete_confirmation.php" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Löschbestätigung erstellen</legend>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Anrede</label>
                        <div class="col-md-2">
                            <select id="select1" name="select1" class="form-control" required>
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="m">Herr</option>
                                <option value="w">Frau</option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="vorname">Vorname</label>
                        <div class="col-md-4">
                            <input id="vorname" name="vorname" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nachname">Nachname</label>
                        <div class="col-md-4">
                            <input id="nachname" name="nachname" placeholder="" class="form-control input-md"
                                   required="" type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="firma">Firma</label>
                        <div class="col-md-4">
                            <input id="firma" name="firma" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="strasse">Straße</label>
                        <div class="col-md-4">
                            <input id="strasse" name="strasse" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="ort">PLZ + Ort</label>
                        <div class="col-md-4">
                            <input id="ort" name="ort" placeholder="" class="form-control input-md" required=""
                                   type="text">

                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="vorstandname">Vorstand</label>
                        <div class="col-md-2">
                            <select id="vorstandname" name="vorstandname" class="form-control" required>
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="Steffen Hock">Steffen Hock</option>
                                <option value="Moritz Beck">Moritz Beck</option>
                                <option value="Florian Helmerich">Florian Helmerich</option>
                            </select>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="submit"></label>
                        <div class="col-md-4">
                            <button id="Reset" type="reset" name="Reset" class="btn btn-danger">Eingaben löschen
                            </button>
                            <button id="submit" name="submit" type="submit" class="btn btn-primary">erstellen</button>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>

</body>

</html>