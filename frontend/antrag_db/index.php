    <?php
    //session_start();

    //setzen der Zeitzone für Datums-Funktionen
    date_default_timezone_set('Europe/Berlin');
    require_once('../rechner_db/inc/db.php');

    require('db_functions.php');

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);


    if (!isset($_SESSION['user_id'])) {
        header("Location: ../../index.php");
    }

    //Abrufen der Benutzerdaten
    require '../login-page/database.php';

    if (isset($_SESSION['user_id'])) {
        $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
        $records->bindParam(':id', $_SESSION['user_id']);
        $records->execute();
        $results = $records->fetch(PDO::FETCH_ASSOC);
        $user = NULL;
        if (count($results) > 0) {
            $user = $results;
        }
    }

    ?>
    <!DOCTYPE html>
    <html lang="de">

    <head>
        <meta charset="ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

        <title>reversy</title>


        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/dashboard.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/search.css" rel="stylesheet">

        <script src="../js/jquery-3.2.0.js"></script>


    </head>

    <body>
    <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../mainpage/index.php"> Angestöpselt - reversy</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                        if ($_SESSION['admin'] == 1) {
                            echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                        }
                        ?>
                        <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                                    echo $extension; //personalisierte Willkommensnachhricht?>!</b></a></li>
                        <li><a href="../login-page/user.php">Profil</a></li>
                        <li><a href="../login-page/logout.php">Abmelden</a></li>
                    </ul>
                    <form class="navbar-form navbar-right">

                    </form>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a></li>
                        <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                        <li class="active"><a href="../antrag_db/index.php">Kunden-DB</a></li>
                    </ul>
                    <br/>
                    <hr/>
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="">Kunden Übersicht</a></li>
                        <li><a href="../antrag_form/index.php">Kunden hinzufügen</a></li>
                    </ul>
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                    <h3 class="sub-header">Kunden-Suche</h3>

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div id="imaginary_container">
                                    <form class="form-horizontal" action="" method="get">
                                        <div class="input-group stylish-input-group">
                                            <input type="text" name="suche" class="form-control"
                                                   value="<?php if (isset($_GET['suche'])) {
                                                       echo $_GET['suche'];
                                                   } ?>" placeholder="Suche nach Kunden (Nummer, Vorname, Nachname)">
                                            <input type="hidden" name="aktion" value="suchen">
                                            <span class="input-group-addon">
                        					<button type="submit"><span
                                                        class="glyphicon glyphicon-search"></span></button>
                            			
											</span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php

            $anzahlNachStatus = new selectDB();

            $count_alle = $anzahlNachStatus->countAll();

            $count_bereit = $anzahlNachStatus->countFromStatus('bereit');

            $count_beschenkt = $anzahlNachStatus->countFromStatus('beschenkt');

            $count_verhindert = $anzahlNachStatus->countFromStatus('verhindert/ will nicht');

            $count_info = $anzahlNachStatus->countFromStatus('info fehlt');

            $count_gesperrt = $anzahlNachStatus->countFromStatus('GESPERRT');

            $count_eingeladen = $anzahlNachStatus->countFromStatus('eingeladen');

            ?>


            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                <h3 class="sub-header">Kunden Übersicht</h3>

                <?php if (isset($_GET['selectstatus'])) {
                    $selectstatus = $_GET['selectstatus'];
                }
                ?>
                <form class="form-horizontal">
                    <fieldset>

                        <?php if (!isset($_GET['aktion'])) { ?>
                        <!-- Suche nach bestimmten Status in der Tabelle-->
                        <div class="form-group">
                            <label class="col-md-1 control-label" for="selectstatus">Status:</label>
                            <div class="col-md-2">
                                <select id="selectstatus" name="selectstatus" class="form-control">
                                    <option value="alle" <?php if (isset($selectstatus) && $selectstatus == "alle") {echo "selected";} ?>>Alle (<?php echo $count_alle; ?>)</option>
                                    <option value="bereit" <?php if (isset($selectstatus) && $selectstatus == "bereit") {echo "selected";} ?>>bereit (<?php echo $count_bereit; ?>)</option>
                                    <option value="beschenkt" <?php if (isset($selectstatus) && $selectstatus == "beschenkt") {echo "selected";} ?>>beschenkt (<?php echo $count_beschenkt; ?>)</option>
                                    <option value="verhindert/ will nicht" <?php if (isset($selectstatus) && $selectstatus == "verhindert/ will nicht") {echo "selected";} ?>>verhindert/ will nicht (<?php echo $count_verhindert; ?>)</option>
                                    <option value="info fehlt" <?php if (isset($selectstatus) && $selectstatus == "info fehlt") {echo "selected";} ?>>Info fehlt (<?php echo $count_info; ?>)</option>
                                    <option value="eingeladen" <?php if (isset($selectstatus) && $selectstatus == "eingeladen") {echo "selected";} ?>>eingeladen (<?php echo $count_eingeladen; ?>)</option>
                                    <option value="GESPERRT" <?php if (isset($selectstatus) && $selectstatus == "GESPERRT") {echo "selected";} ?>>GESPERRT (<?php echo $count_gesperrt; ?>)</option>
                                </select>
                                <input type="hidden" name="aktion" value="statusSelect">
                            </div>
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </fieldset>
                </form>
            <?php
            $selectstatus = '';
            }
            ?>

                <!-- Database Table:-->
                <script src="jquery.min.js"></script>
                <script src="../js/bootstrap.min.js"></script>

                <div class="container-fluid">
                    <div class="row">


                <div class="col-md-12">
                    <div class="table-responsive">


                        <table id="mytable" class="table table-bordred table-striped table-hover">

                            <thead>

                            <th>Kunde</th>
                            <th>Antrag am</th>
                            <th>Via</th>
                            <th>Name</th>
                            <th>Anschrift</th>
                            <th>Telefon</th>
                            <th>Mobil</th>
                            <th>Mail</th>
                            <th>Bemerkungen</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            </thead>


        <?php

        $Nachweis_Am = "";

        $heute = date("Y-m-d");

        //Übergabe und Prüfung der Formulardaten wenn Datensätze bearbeitet wurden.
        if (isset($_POST['aktion']) and $_POST['aktion'] == 'korrigieren') {

            $id = "";
            if (isset($_POST['id'])) {$id = trim(htmlspecialchars($_POST['id']));}

            $Via = "";
            if (isset($_POST['Via'])) {$Via = trim(htmlspecialchars($_POST['Via']));}

            $Anrede = "";
            if (isset($_POST['Anrede'])) {$Anrede = trim(htmlspecialchars($_POST['Anrede']));
            }
            $Nachweis_Typ = "";
            if (isset($_POST['Nachweis_Typ'])) {$Nachweis_Typ = trim(htmlspecialchars($_POST['Nachweis_Typ']));}

            $Nachweis_Am = "";
            if (isset($_POST['Nachweis_Am'])) {$Nachweis_Am = trim(htmlspecialchars($_POST['Nachweis_Am']));}

            $Vorname = "";
            if (isset($_POST['Vorname'])) {$Vorname = trim(htmlspecialchars(utf8_encode($_POST['Vorname'])));}

            $Nachname = "";
            if (isset($_POST['Nachname'])) {$Nachname = trim(htmlspecialchars(utf8_encode($_POST['Nachname'])));}

            $Anschrift = "";
            if (isset($_POST['Anschrift'])) {$Anschrift = trim(htmlspecialchars(utf8_encode($_POST['Anschrift'])));}

            $Telefon = "";
            if (isset($_POST['Telefon'])) {$Telefon = trim(htmlspecialchars($_POST['Telefon']));}

            $Mobil = "";
            if (isset($_POST['Mobil'])) {$Mobil = trim(htmlspecialchars($_POST['Mobil']));}

            $Email = "";
            if (isset($_POST['Email'])) {$Email = trim(htmlspecialchars($_POST['Email']));}

            $Status = "";
            if (isset($_POST['Status'])) {$Status = trim(htmlspecialchars($_POST['Status']));}

            $Anmerkung = "";
            if (isset($_POST['Anmerkung'])) {$Anmerkung = trim(htmlspecialchars(utf8_encode($_POST['Anmerkung'])));}

            $Nachweis_vorhanden = "";
            if (isset($_POST['Nachweis_vorhanden'])) {$Nachweis_vorhanden = trim(htmlspecialchars($_POST['Nachweis_vorhanden']));}

            $Abhol_Datum = "";
            if (isset($_POST['Abhol_Datum'])) {$Abhol_Datum = trim(htmlspecialchars($_POST['Abhol_Datum']));}


            //Prüfung, ob ein Nachweis erbracht wurde.
            if ($Nachweis_Typ != "nein" and $Nachweis_vorhanden == "ja") {
                $Nachweis_Am = $heute;
            }
            //Automatisches setzten des Status je nach Situation
            if ($Nachweis_Am == "") {
                $Status = "info fehlt";
            }

            if ($Nachweis_Am != 0 and $Status != "GESPERRT" and $Status != "verhindert/ will nicht") {
                $Status = "bereit";
            }


            if ($Status != '') {
                // speichern des geänderten Datensatzes


                $updateKunden = new updateDB();

                $ergebnis = $updateKunden->updateAntrag($id, $Via, $Anrede, $Nachweis_Typ, $Nachweis_Am, $Vorname, $Nachname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Abhol_Datum);

            }

            ?>
            <div class="alert alert-success alert-dismissable">
                <a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Kunden-Datensatz <?php echo $id; ?> wurde geändert!</strong>
            </div>

            <?php


        }
        $modus_aendern = false;

        //Erezugung eines Formulars, indem die aktuellen Datensätze dargestellt werden, diese können dann bearbeitet werden.
        if (isset($_GET['aktion']) and $_GET['aktion'] == 'bearbeiten') {
        $modus_aendern = true;
        if (isset($_GET['id'])) {
        $id = (INT)$_GET['id'];
        if ($id > 0) {

            $zuBearbeitendeDatensaetzeEinlesen = new selectDB();

            $ergebnis = $zuBearbeitendeDatensaetzeEinlesen->selectOneDataset($id);

        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <!-- In diesem Formular werden die Werte der Felder automatisch durch abfragen der einzelnen Optionen gesetzt.
        Aufgrund von unterschiedlichen Schreibweisen in der "alten" Excel Tabelle kann es dazu kommen, dass gewisse Werte nicht richtig angezeigt werden.-->
        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" accept-charset="utf-8"
        <fieldset>
        </div>
        </div>
        <br><br>

        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Via">Via:</label>
        <div class="col-md-4">
        <select id="Via" name="Via" class="form-control" required="">
            <option value="Pers" <?php if ($ergebnis[2] == "Pers") {echo "selected";} ?>>Pers</option>
            <option value="Brief" <?php if ($ergebnis[2] == "Brief") {echo "selected";} ?>>Brief</option>
            <option value="Email" <?php if ($ergebnis[2] == "Email") {echo "selected";} ?>>Email</option>
            <option value="Tel" <?php if ($ergebnis[2] == "Tel") {echo "selected";} ?>>Tel</option>
            <option value="Ab/Fax" <?php if ($ergebnis[2] == "Ab/Fax") {echo "selected";} ?>>Ab/Fax</option>
        </select>
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachweis_Typ">Nachweis Typ:</label>
        <div class="col-md-4">
        <select id="Nachweis_Typ" name="Nachweis_Typ" class="form-control" required="">
            <option value="nein" <?php if ($ergebnis[3] == "nein") {echo "selected";} ?>>noch kein Nachweis</option>
            <option value="ALGII" <?php if ($ergebnis[3] == "ALGII") {echo "selected";} ?>>ALGII</option>
            <option value="Asyl" <?php if ($ergebnis[3] == "Asyl") {echo "selected";} ?>>Asyl</option>
            <option value="Schüler" <?php if ($ergebnis[3] == "Schüler") {echo "selected";} ?>>Schüler</option>
            <option value="Rente" <?php if ($ergebnis[3] == "Rente") {echo "selected";} ?>>Rentner</option>
            <option value="gemeinnützige Org." <?php if ($ergebnis[3] == "gemeinnützige Org.") {echo "selected";} ?>>gemeinnützige Org.</option>
            <option value="Schwerbehindertenausweis" <?php if ($ergebnis[3] == "Schwerbehindertenausweis") {echo "selected";} ?>>Schwerbehindertenausweis</option>
        </select>
        </div>
        </div>
        <br><br>
        <!-- Multiple Radios (inline) -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachweis_vorhanden">Nachweis
        gesehen:</label>
        <div class="col-md-4">
        <label class="radio-inline" for="Nachweis_vorhanden-0">
            <input name="Nachweis_vorhanden" id="Nachweis_vorhanden0" value="ja" type="radio" <?php if ($ergebnis[4] != 0) echo 'checked="checked"';} ?>>Ja
        </label>
        <label class="radio-inline" for="Nachweis_vorhanden-1">
            <input name="Nachweis_vorhanden" id="Nachweis_vorhanden1" value="nein" type="radio" <?php if ($ergebnis[4] == 0) {echo 'checked="checked"';} ?>>Nein
        </label>
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anrede">Anrede:</label>
        <div class="col-md-4">
        <select id="Anrede" name="Anrede" class="form-control" required="">
            <option value="Herr" <?php if ($ergebnis[5] == "Herr") {echo "selected";} ?>>Herr</option>
            <option value="Frau" <?php if ($ergebnis[5] == "Frau") {echo "selected";} ?>>Frau</option>
        </select>
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Vorname">Vorname:</label>
        <div class="col-md-4">
        <input id="Vorname" name="Vorname" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $ergebnis[7]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachname">Nachname:</label>
        <div class="col-md-4">
        <input id="Nachname" name="Nachname" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $ergebnis[6]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anschrift">Anschrift:</label>
        <div class="col-md-4">
        <input id="Anschrift" name="Anschrift" placeholder="Str. PLZ Ort" class="form-control input-md" required="" type="text" value="<?php echo $ergebnis[8]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Telefon">Telefon:</label>
        <div class="col-md-4">
        <input id="Telefon" name="Telefon" placeholder="" class="form-control input-md" type="text" value="<?php echo $ergebnis[9]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Mobil">Mobil: </label>
        <div class="col-md-4">
        <input id="Mobil" name="Mobil" placeholder="" class="form-control input-md" type="text" value="<?php echo $ergebnis[10]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Email">Email:</label>
        <div class="col-md-4">
        <input id="Email" name="Email" placeholder="" class="form-control input-md" type="text" value="<?php echo $ergebnis[11]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Status">Status:</label>
        <div class="col-md-4">
        <select id="Status" name="Status" class="form-control" required="">
            <option value="bereit" <?php if ($ergebnis[12] == "bereit") {echo "selected";} ?>>bereit</option>
            <option value="info fehlt" <?php if ($ergebnis[12] == "info fehlt") {echo "selected";} ?>>info fehlt</option>
            <option value="verhindert/ will nicht" <?php if ($ergebnis[12] == "verhindert/ will nicht") {echo "selected";} ?>>verhindert/ will nicht</option>
            <option value="GESPERRT" <?php if ($ergebnis[12] == "GESPERRT") {echo "selected";} ?>>GESPERRT</option>
        </select>
        </div>
        </div>
        <br><br>
        <!-- Textarea -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anmerkung">Anmerkungen:</label>
        <div class="col-md-4">
        <textarea class="form-control" id="Anmerkung" name="Anmerkung"><?php echo $ergebnis[13]; ?></textarea>
        </div>
        </div>
        <br>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Abhol_Datum">Abhol Datum:</label>
        <div class="col-md-4">
        <input id="Abhol_Datum" name="Abhol_Datum" placeholder="" class="form-control input-md" type="text" value="<?php echo $ergebnis[15]; ?>">
        </div>
        </div>
        <br><br>

        <label class="col-md-4 control-label" for="Reset"></label>
        <div class="col-md-8">
        <a href="index.php" class="btn btn-danger">zurück/schließen</a>

        <input type="hidden" name="id" value="<?php echo $ergebnis[0]; ?>">
        <input type="hidden" name="aktion" value="korrigieren">
        <button id="submit" type="submit" name="submit" class="btn btn-primary" formmethod="post" formaction="">Änderungen speichern</button>
        </div>
        </div>
        </div>
        </fieldset>
        </form>
        <hr>

        <?php
        }
        }




        //Dieses Formular gibt eine Übersicht ALLER Daten des jeweiligen Datensatzes per view aus. Hier sind keine Änderungen für den Nutzer möglich.
        if (isset($_GET['aktion']) and $_GET['aktion'] == 'view') {
        $modus_aendern = true;
        if (isset($_GET['id'])) {
        $id = (INT)$_GET['id'];
        if ($id > 0) {

        $viewEinlesen = new selectDB();

        $ergebnis = $viewEinlesen->selectOneDataset($id);
        }

        ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <!-- Vereinfachtes Fomular ohne besondere Auswahlmöglichkieten-->
        <form class="form-horizontal" action="" method="post">
        <fieldset>
        </div>
        </div>
        <br><br>

        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Via">Via:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[2]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachweis_Typ">Nachweis Typ:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[3]; ?>">
        </div>
        </div>
        <br><br>
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachweis_Am">Nachweis Am:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[4]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anrede">Anrede:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[5]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Vorname">Vorname:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[7]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Nachname">Nachname:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[6]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anschrift">Anschrift:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[8]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Telefon">Telefon:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[9]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Mobil">Mobil: </label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[10]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Email">Email:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[11]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Status">Status:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[12]; ?>">
        </div>
        </div>
        <br><br>
        <!-- Textarea -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anmerkung">Anmerkungen:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[13]; ?>">
        </div>
        </div>
        <br>
        <br><br>
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anmerkung">Ausgabe Datum:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[14]; ?>">
        </div>
        </div>
        <br>
        <br><br>
        <div class="form-group">
        <label class="col-md-4 control-label" for="Abhol_Datum">Abhol Datum:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[15]; ?>">
        </div>
        </div>
        <br>
        <br><br>
        <div class="form-group">
        <label class="col-md-4 control-label" for="appendedtext">Anlage:</label>
        <div class="col-md-4">
        <div class="input-group">
        <input class="form-control" value="<?php echo $ergebnis[16]; ?>" disabled type="text"><?php if ($ergebnis[16] != 0) { ?>
        <span class="input-group-addon">
        <!--Dieses Eingabefeld verfügt über die Möglichkeit sich den ausgeggebenen Rechner per Verlinkung direkt in der Rechner Datenbank anzusehen-->
        <a href="../rechner_db/suche.php?rechner=<?php echo $ergebnis[16]; ?>" target="_blank" <span class="glyphicon glyphicon-zoom-in"></span></a></span>
        <?php } ?>
        </div>
        </div>
        </div>
        <br>
        <br><br>
        <?php if ($_SESSION['admin'] == 1) { ?>

        <div class="form-group">
        <label class="col-md-4 control-label" for="Anmerkung">Mitarbeiter:</label>
        <div class="col-md-4">
        <input class="form-control input-md" type="text" disabled value="<?php echo $ergebnis[18]; ?>">
        </div>
        </div>
        <br>
        <br><br>
        <?php } ?>
        <!-- Button (Double) -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Reset"></label>
        <div class="col-md-8">
        <a href="index.php" class="btn btn-primary">zurück/schließen</a>
        </div>
        </div>
        </div>
        <br><br>
        </fieldset>
        </form>
        <hr>

        <?php
        }
            }

        //Hier werden die Rechner "ausgegeben". Es wird hierbei die dazugehörige Rechnernummer zum passenden Datensatz hinzugefügt, wenn der Datensatz alle Kriterien erfüllt. Das passende Formular dazu s.u.
        if (isset($_POST['aktion']) and $_POST['aktion'] == 'ausgeben') {
        $id = "";
        if (isset($_GET['id'])) {
        $id = (INT)trim($_GET['id']);
        }
        $Anlage = "";
        if (isset($_POST['Anlage'])) {
        $Anlage = trim($_POST['Anlage']);
        }
        $Ausgabe_Datum = "";
        if (isset($_POST['Ausgabe_Datum'])) {
        $Ausgabe_Datum = $_POST['Ausgabe_Datum'];
        }

        $Status = "";
        $Status = $_POST['Status'];

        if ($Anlage > 0) {
        // speichern
        $update = $db->prepare("UPDATE antrag SET Status=?, Ausgabe_Datum=?, Anlage=? WHERE id=? LIMIT 1;");
        $update->bind_param('sssi', $Status, $Ausgabe_Datum, $Anlage, $id);
        if ($update->execute()) {
        ?>
        <div class="alert alert-success alert-dismissable">
        <a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Rechner <?php echo $Anlage; ?> wurde erfolgreich ausgegeben!</strong>
        </div>

        <?php
        }
            }
                }

        //Hier wird das Datum der Einladung zum Abholen des Rechners und die Bestätigung in der Datenbank gespeichert

        if (isset($_POST['aktion']) and $_POST['aktion'] == 'ausgebenFestnetz') {

            $id = "";
            if (isset($_GET['id'])) {
                $id = (INT)trim($_GET['id']);
            }

            $AbholDatum = "";
            if (isset($_POST['abholDatum'])) {
                $AbholDatum = trim($_POST['abholDatum']);
            }

            $eingeladenBestaetigung = "";
            if (isset($_POST['eingeladenBestaetigung'])) {
                $eingeladenBestaetigung = trim($_POST['eingeladenBestaetigung']);
            }


            if($eingeladenBestaetigung == "ja"){

                $ausgabeFestnetz = new updateDB();

                $ergebnis = $ausgabeFestnetz->updateAusgabeKontakt($id, $AbholDatum);

                if($ergebnis != "") {

                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Kunde wurde erfolgreich als eingeladen markiert!</strong>
                    </div>

                    <?php
                }
            }

        }


        // Formular für die Ausgabe der Rechner.
        if (isset($_GET['aktion']) and $_GET['aktion'] == 'ausgabe') {
        $modus_aendern = true;
        if (isset($_GET['id'])) {
        $id_einlesen = (INT)$_GET['id'];
        if ($id_einlesen > 0)
        {
        $dseinlesen = $db->prepare("SELECT id, Anrede, Vorname, Nachname, Nachweis_Typ, Nachweis_Am, Telefon, Mobil, Email, Status, Ausgabe_Datum, Abhol_Datum, Anlage, Mitarbeiter FROM antrag WHERE id = ? ");
        $dseinlesen->bind_param('i', $id_einlesen);
        $dseinlesen->execute();
        $dseinlesen->bind_result($ID, $Anrede, $Vorname, $Nachname, $Nachweis_Typ, $Nachweis_Am, $Telefon, $Mobil, $Email, $Status, $Ausgabe_Datum, $Abhol_Datum, $Anlage, $Mitarbeiter);
        while ($dseinlesen->fetch()) {

        ?>


        <!-- Einladung zum Abholen der Rechner für Kunden mit Handy Nummer mit automatischem Versand durch Messagebird. -->
        <?php if ($Mobil != 0 or $Telefon == 0) { ?>
        <form class="form-horizontal" action="sms.php" method="post">
        <fieldset>

        <!-- Form Name -->
        <legend>Einladen (Handy) <?php echo ' - ' . $Anrede . ' ' . $Vorname . ' ' . $Nachname . ' Nr: ' . $ID; ?></legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="handyNr">Handy Nr.</label>
            <div class="col-md-4">
                <input id="handyNr" name="handyNr" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $Mobil; ?>" <?php if ($Status == "eingeladen") {echo "disabled";} ?>>
                <span class="help-block"> Bitte achte auf folgendes Format: +49xxxxxxxxxxx</span>
            </div>
        </div>
        <br>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="abholDatum">Abholzeitraum</label>
            <div class="col-md-4">
                <input id="abholDatum" name="abholDatum" placeholder="zb. 01.07.2020 - 07.07.2020" class="form-control input-md" required="" type="date" value="<?php echo $Abhol_Datum; ?>"<?php if ($Status == "eingeladen") {echo "disabled";} ?>>
                <span class="help-block">zb. 01.07.2020 - 07.07.2020 (~ca. 7 Tage)</span>
            </div>
        </div>
        <br>
        <br><br>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="sendSMS">SMS verschicken</label>
            <div class="col-md-4">
                <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                <button type="submit" id="sendSMS" formmethod="post" formaction="sms.php" class="btn btn-primary" <?php if ($Status == "eingeladen") {echo "disabled";} ?>><span class="glyphicon glyphicon-send"></span></button>
            </div>
        </div>
        </fieldset>
        </form>
        <?php }
        if ($Telefon != 0 and $Mobil == "") { ?>

        <!-- Einladung zum Abholen der Rechner für Kunden mit Festnetznummer mit manueller Bestätigung der Einladung und eintragen des Tages. -->
        <form class="form-horizontal" action="" method="post">
        <fieldset>

        <!-- Form Name -->
        <legend>Einladen (Festnetz)</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="festnetzNr">Festnetz Nr.</label>
            <div class="col-md-4">
                <input id="festnetzNr" name="festnetzNr" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $Telefon; ?>" <?php if ($Status == "eingeladen") {echo "disabled";} ?>>
            </div>
        </div>
        <br>
        <br><br>
        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="abholDatum">Abholzeitraum</label>
            <div class="col-md-4">
                <input id="abholDatum" name="abholDatum" placeholder="zb. 01.07.2020 - 07.07.2020" class="form-control input-md" required="" type="date" value="<?php echo $Abhol_Datum; ?>" <?php if ($Status == "eingeladen") {echo "disabled";} ?>>
                <span class="help-block">zb. 01.07.2020 - 07.07.2020 (~ca. 7 Tage)</span>
            </div>
        </div>
        <br>
        <br><br>
        <!-- Multiple Radios (inline)-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="eingeladenBestaetigung">Kunde
                angerufen</label>
            <div class="col-md-4">
                <label class="radio-inline" for="eingeladenBestaetigung-0">
                    <input name="eingeladenBestaetigung" id="eingeladenBestaetigung-0" value="ja" required type="radio" <?php if ($Status == "eingeladen") {echo "disabled";} ?>>
                    ja
                </label>
                <label class="radio-inline" for="eingeladenBestaetigung-1">
                    <input name="eingeladenBestaetigung" id="eingeladenBestaetigung-1" value="nein" required type="radio" <?php if ($Status == "eingeladen") {echo "disabled";} ?>>
                    nein
                </label>
            </div>
        </div>

        <br>
        <br><br>
        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <input type="hidden" name="aktion" value="ausgebenFestnetz">
                <button id="submit" type="submit" name="submit" class="btn btn-primary" <?php if ($Status == "eingeladen") {echo "disabled";} ?> formmethod="post" formaction="">abschicken</button>
            </div>
        </div>
        </fieldset>
        </form>
        <?php }
        if ($Mobil == "" && $Telefon == "") {
        echo "Keine telefonischen Kontaktdaten angegeben, Mail: " . $Email;
        }
        ?>


        <!-- Eintragung der Rechnernummer direkt bei der Ausgabe -->
        <hr>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <form class="form-horizontal" action="" method="post">
        <fieldset>
        </div>
        </div>
        <br><br>

        <!-- Select Basic -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Anlage">Anlage-Nr:</label>
        <div class="col-md-4">
        <input id="Anlage" name="Anlage" placeholder="" class="form-control input-md" required="" type="text" value="<?php if ($Anlage != 0) {echo $Anlage;} ?>">
        </div>
        </div>
        <br><br>

        <!-- Button (Double) -->
        <div class="form-group">
        <label class="col-md-4 control-label" for="Reset"></label>
        <div class="col-md-8">
        <a href="index.php" class="btn btn-danger">zurück/schließen</a>


        <input type="hidden" name="id" value="<?php $ID ?>">
        <input type="hidden" name="Status" value="<?php echo $Status = "beschenkt"; ?>">
        <input type="hidden" name="Ausgabe_Datum" value="<?php echo date("Y-m-d"); ?>">
        <input type="hidden" name="aktion" value="ausgeben">
        <button type="submit" class="btn btn-warning" formmethod="post" formaction="">Rechner ausgeben</button>

        </div>
        </div>
        </div>
        <br><br>
        </fieldset>
        </form>
        <hr>




        <?php
        }
            }
                }
                    }


        //Folgender Banner wird gezeigt, wenn die SMS erfolgreich versendet wurde (sms.php). Ansonsten wird der Error auf der Seite sms.php dargestellt.
        if(isset($_GET['aktion']) && $_GET['aktion'] == "smsVerschickt") {?>
            <div class="alert alert-success alert-dismissable">
                <a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>SMS wurde erfolgreich versendet!</strong>
            </div>
        <?php
        }

        //Prüft, ob nach Status selektiert werden soll
        if (isset($_GET['aktion'])) {
        $aktionStatus = $_GET['aktion'];
        }
        if (isset($aktionStatus) && $aktionStatus === 'statusSelect') {
        if (isset($_GET['selectstatus'])) {
        $selectstatus = $_GET['selectstatus'];
        }

        if ($selectstatus == 'alle') {
        $erg = $db->query("SELECT * FROM antrag ORDER BY id DESC LIMIT 40")
        or die($db->error);
        } else {
        $erg = $db->query("SELECT * FROM antrag WHERE Status = '$selectstatus' ORDER BY id DESC")
        or die($db->error);
        }

        //Wenn nicht nach Status sortiert wird, wird dieser Block ausgegeben:
        } else {

        if (isset($_GET['anzeigen'])) {
        $anzeigen = $_GET['anzeigen'];
        } else {
        $anzeigen = "n";
        }



        //Ausgabe der Daten als Tabelle
        //Zusätzlich Unterscheidung wie viele Datensätze aus der Tabelle geladen werden, dazu Link am Tabellenende.
        if ($anzeigen === 'j') {
        //Suchfunktion die wahlweise, wenn eine Suche eingegeben ist die SQL Abfrage, um die Tabelle auszugeben verändert.
        if (isset($_GET['aktion']) and $_GET['aktion'] == 'suchen' and !empty($_GET['suche'])) {

        $suche = "";
        $suche = htmlspecialchars($_GET["suche"]);
        //veränderte Abfrage bei der nach der Sucheingabe durch Volltextsuche gesucht wird
        $erg = $db->query("SELECT * FROM antrag WHERE MATCH (Vorname, Nachname) AGAINST ('$suche' IN NATURAL LANGUAGE MODE) OR id = '$suche'")
        or die($db->error);

        } else {
        //normale Ausgabe aller Datensätze der Tabelle
        $erg = $db->query("SELECT * FROM antrag ORDER BY id DESC")
        or die($db->error);
        }
        } else {
        //Suchfunktion die wahlweise, wenn eine Suche eingegeben ist die SQL Abfrage, um die Tabelle auszugeben verändert.
        if (isset($_GET['aktion']) and $_GET['aktion'] == 'suchen' and !empty($_GET['suche'])) {

        $suche = "";
        $suche = htmlspecialchars($_GET["suche"]);
        //veränderte Abfrage bei der nach der Sucheingabe durch Volltextsuche gesucht wird
        $erg = $db->query("SELECT * FROM antrag WHERE MATCH (Vorname, Nachname) AGAINST ('$suche' IN NATURAL LANGUAGE MODE) OR id = '$suche'")
        or die($db->error);

        } else {
        //normale Ausgabe aller Datensätze der Tabelle
        $erg = $db->query("SELECT * FROM antrag ORDER BY id DESC LIMIT 50")
        or die($db->error);
        }
        }
        }
        // while ($zeile = $erg->fetch_assoc()) {
        while ($zeile = $erg->fetch_object()) {
        ?>

        <tr>

        <td>

        <?php echo $zeile->id; ?>
        </td>
        <td>
        <?php echo $zeile->Eingang_Datum; ?>
        </td>
        <td>
        <?php echo $zeile->Via; ?>
        </td>
        <td>
        <?php echo "$zeile->Anrede $zeile->Vorname $zeile->Nachname"; ?>
        </td>
        <td>
        <?php echo $zeile->Anschrift; ?>
        </td>
        <td>
        <?php echo $zeile->Telefon; ?>
        </td>
        <td>
        <?php echo $zeile->Mobil; ?>
        </td>
        <td>
        <a href="mailto:<?php echo $zeile->Email; ?>?subject=Angestöpselt e.V."><?php echo $zeile->Email; ?></a>

        </td>
        <td>
        <?php echo $zeile->Anmerkung; ?>
        </td>
        <td>
        <?php //verschiedene icons je nach Status des Kunden
        if ($zeile->Status == "bereit") {
        echo '<span class="glyphicon glyphicon-ok" style="color:#269900">' . '</span>';
        } elseif ($zeile->Status == "beschenkt") {
        echo '<span class="glyphicon glyphicon-ok" style="color:#E9CC2F"">' . '</span>';
        } elseif ($zeile->Status == "eingeladen") {
            echo '<span class="glyphicon glyphicon-ok" style="color:#4568e9"">' . '</span>';
        } elseif ($zeile->Status == "GESPERRT") {
        echo '<span class="glyphicon glyphicon-remove" style="color:#E50000"">' . '</span>';
        } else {
        echo '<span class="glyphicon glyphicon-remove">' . '</span>';
        }

        ?>
        </td>
        <td>
        <a href="?aktion=bearbeiten&id=<?php echo $zeile->id; ?>"
        class='btn btn-primary btn-xs <?php if ($zeile->Status == "beschenkt") {
        echo "disabled";
        } ?>' data-title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>
        </td>
        <td>
        <a href="?aktion=ausgabe&id=<?php echo $zeile->id; ?>"
        class='btn btn-warning btn-xs <?php if ($zeile->Ausgabe_Datum != 0 or $zeile->Status != "eingeladen" and $zeile->Status != "bereit") {
        echo "disabled";
        } ?>' data-title='Ausgabe'><span class='glyphicon glyphicon-new-window'></span></a>
        </td>
        <td>
        <a href="?aktion=view&id=<?php echo $zeile->id; ?>" class='btn btn-info btn-xs'
        data-title='View'><span class='glyphicon glyphicon-eye-open'></span></a>
        </td>
        </tr>
        <?php
        }
        echo "</table>";

        $erg->free();
        $db->close();

        //Bestimmt ob alle Datensätze aus dieser Tabelle geladen werden sollen.
        if (!isset($_GET['anzeigen'])) {
        echo "<a href='?anzeigen=j'>Alle Datensätze anzeigen</a>";
        }
        ?>


        </div>
            </div>
                </div>
                    </div>
                        </div>
                            </div>
                                </div>
                                    </div>
                                        </div>
                                            </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
        </script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        </body>

</html>