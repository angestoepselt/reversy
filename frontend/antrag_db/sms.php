<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}else {

require(__DIR__ . '/../../config.php');
require('db_functions.php');

//Formularprüfung des SMS-send Formulars

    $handyNr = "";
    if (isset($_POST['handyNr'])) {
        $handyNr = trim($_POST['handyNr']);
    }

    $AbholDatum = "";
    if (isset($_POST['abholDatum'])) {
        $AbholDatum = trim($_POST['abholDatum']);
    }

    $aktion = "";
    if (isset($_GET['aktion'])) {
        $aktion = trim($_GET['aktion']);
    }

    $id = "";
    if (isset($_POST['id'])) {
        $id = trim($_POST['id']);
    }


//Kommunikation mit der Messagebird REST-API
    require 'vendor/messagebird/php-rest-api/autoload.php';

    $MessageBird = new \MessageBird\Client($smsKey);

    $Message = new \MessageBird\Objects\Message();
    $Message->originator = 'PC-Spende';
    $Message->recipients = array($handyNr);
    $Message->body = 'Ihr beantragter Computer steht vom '.$AbholDatum.' jeweils Montags und Mittwochs ab 18:30 in der Frankfurter Str. 74 für Sie bereit. WICHTIG: Bitte bringen Sie 10€ Bearbeitungsgebühr mit! Bitte antworten Sie NICHT auf diese SMS! Sie erreichen uns zu den oben genannten Zeiten telefonisch: 093132091494';

    try {
        $MessageResult = $MessageBird->messages->create($Message);
        //var_dump($MessageResult);

        $eingeladen = new updateDB();

        $execute = $eingeladen->updateAusgabeKontakt($id,$AbholDatum);

        header("Location: index.php?aktion=smsVerschickt");


    } catch (\MessageBird\Exceptions\AuthenticateException $e) {
        // That means that your accessKey is unknown
        echo 'wrong login';

    } catch (\MessageBird\Exceptions\BalanceException $e) {
        // That means that you are out of credits, so do something about it.
        echo 'no balance';

    } catch (\Exception $e) {
        echo $e->getMessage();
    }
}
?>
