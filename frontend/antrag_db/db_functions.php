<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//setzen der Zeitzone für Datums-Funktionen
date_default_timezone_set('Europe/Berlin');
require_once('../rechner_db/inc/db.php');

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}else{

//Inserts data in the DB
    class insertDB
    {

        public function insert($ID, $Eingang_Datum, $Via, $Nachweis_Typ, $Nachweis_Am, $Anrede, $Nachname, $Vorname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Mitarbeiter)
        {
            require('../rechner_db/inc/db.php');

            $sql = "INSERT INTO antrag (id, Eingang_Datum, Via, Nachweis_Typ, Nachweis_Am, Anrede, Nachname, Vorname, Anschrift, Telefon, Mobil, Email, Status, Anmerkung, Mitarbeiter) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $kommando = mysqli_prepare($db, $sql);
            mysqli_stmt_bind_param($kommando, "issssssssssssss", $ID, $Eingang_Datum, $Via, $Nachweis_Typ, $Nachweis_Am, $Anrede, $Nachname, $Vorname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Mitarbeiter);
            mysqli_stmt_execute($kommando);
            $id = mysqli_insert_id($db);

            return "Kunde $id erfolgreich eingetragen";

        }

    }


//Klasse um Daten aus der Tabelle Antrag zu verändern.
    class updateDB
    {
        //Verändert einen kompletten Datensatz bei Angabe aller Werte als input.
        public function update($id, $Eingang_Datum, $Via, $Nachweis_Typ, $Nachweis_Am, $Anrede, $Nachname, $Vorname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Ausgabe_Datum, $Abhol_Datum, $Anlage, $Import_Fehler, $Mitarbeiter)
        {
            require('../rechner_db/inc/db.php');

            $update = $db->prepare("UPDATE antrag SET Eingang_Datum=?, Via=?, Anrede=?, Nachweis_Typ=?, Nachweis_Am=?, Vorname=?, Nachname=?, Anschrift=?, Telefon=?, Mobil=?, Email=?, Status=?, Anmerkung=?, Ausgabe_Datum=?, Abhol_Datum=?, Anlage=?, Import_Fehler=?, Mitarbeiter=? WHERE id=? LIMIT 1;");
            $update->bind_param('ssssssssssssssssssi', $Eingang_Datum, $Via, $Anrede, $Nachweis_Typ, $Nachweis_Am, $Vorname, $Nachname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Ausgabe_Datum, $Abhol_Datum, $Anlage, $Import_Fehler, $Mitarbeiter, $id);
            $update->execute();

            return "Datensatz $id wurde erfolgreich geaendert!";
        }


        //Verändert einen Datensatz nach den Beürfnisen des Update Fomrulars (nur bestimmte Werte werden geupdatet).
        public function updateAntrag($id, $Via, $Anrede, $Nachweis_Typ, $Nachweis_Am, $Vorname, $Nachname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Abhol_Datum)
        {
            require('../rechner_db/inc/db.php');

            $update = $db->prepare("UPDATE antrag SET Via=?, Anrede=?, Nachweis_Typ=?, Nachweis_Am=?, Vorname=?, Nachname=?, Anschrift=?, Telefon=?, Mobil=?, Email=?, Status=?, Anmerkung=?, Abhol_Datum=? WHERE id=? LIMIT 1;");
            $update->bind_param('sssssssssssssi', $Via, $Anrede, $Nachweis_Typ, $Nachweis_Am, $Vorname, $Nachname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Abhol_Datum, $id);
            if ($update->execute()) {

                return "Datensatz $id wurde erfolgreich geaendert!";
            }


        }


        //Diese Funktion trägt alle notwendigen Daten ein, sobald ein Kunde angerufen/ per SMS kontaktiert wurde.
        public function updateAusgabeKontakt($id, $Abhol_Datum)
        {

            require('../rechner_db/inc/db.php');

            $Status = "eingeladen";

            $update = $db->prepare("UPDATE antrag SET Status=?, Abhol_Datum=? WHERE id=? LIMIT 1;");
            $update->bind_param('ssi', $Status, $Abhol_Datum, $id);
            if ($update->execute()) {

                return "Kunde $id wurde erfolgreich als kontaktiert markiert!";

            }
        }

    }


//Klasse um Daten aus der Tabelle antrag auszulesen (nur lesen).
    class selectDB
    {
        //Ausgabe eines Datensatzes je nach id.
        public function selectOneDataset($id)
        {
            require('../rechner_db/inc/db.php');

            $idSelect = $db->prepare("SELECT * FROM antrag WHERE id = ? ");
            $idSelect->bind_param('i', $id);
            $idSelect->execute();
            $idSelect->bind_result($ID, //[0]
                $Eingang_Datum, //[1]
                $Via,   //[2]
                $Nachweis_Typ,  //[3]
                $Nachweis_Am,   //[4]
                $Anrede,    //[5]
                $Nachname,  //[6]
                $Vorname,   //[7]
                $Anschrift, //[8]
                $Telefon,   //[9]
                $Mobil, //[10]
                $Email, //[11]
                $Status,    //[12]
                $Anmerkung, //[13]
                $Ausgabe_Datum, //[14]
                $Abhol_Datum,   //[15]
                $Anlage,    //[16]
                $Import_Fehler, //[17]
                $Mitarbeiter);  //[18]
            $idSelect->fetch();

            return array($ID, $Eingang_Datum, $Via, $Nachweis_Typ, $Nachweis_Am, $Anrede, $Nachname, $Vorname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Ausgabe_Datum, $Abhol_Datum, $Anlage, $Import_Fehler, $Mitarbeiter);
        }


        //Zählt bestimmte Datensätze (je nach gegebenem Status) aus antrag und gibt diese als $anzahl zurück.
        public function countFromStatus($status)
        {

            require('../rechner_db/inc/db.php');

            $temp = $db->query("SELECT COUNT(*) as anzahl FROM antrag WHERE Status = '$status'") or die($mysqli->error);
            $z = $temp->fetch_array();
            $anzahl = $z["anzahl"];

            return $anzahl;

        }


        //Zählt alle Datensätze aus antrag und gibt diese als $anzahl zurück.
        public function countAll()
        {

            require('../rechner_db/inc/db.php');

            $temp = $db->query("SELECT COUNT(*) as anzahl FROM antrag") or die($mysqli->error);
            $z = $temp->fetch_array();
            $anzahl = $z["anzahl"];

            return $anzahl;

        }
    }

    /*
    $id = "24";
    $AbholDatum = "";

    $eingeladen = new updateDB();

    $test = $eingeladen->updateAusgabeKontakt($id,$AbholDatum);

    echo $test;

    $test = new updateDB();

    $ergebnis = $test->updateAusgabeKontakt('8','30.01.2017-12.07.2018');
    echo $ergebnis;*/

}
?>

