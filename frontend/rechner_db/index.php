<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/search.css" rel="stylesheet">


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
            <form class="navbar-form navbar-right">

            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a></li>
                <li class="active"><a href="#">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li class="active"><a href="">Rechner Übersicht</a></li>
                <li><a href="../rechner_form/index.php">Rechner eintragen</a></li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h3 class="sub-header">Rechner-Datensatz Suche</h3>

            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div id="imaginary_container">
                            <form class="form-horizontal" action="suche.php" method="post">
                                <div class="input-group stylish-input-group input-append">
                                    <input type="text" name="suche" class="form-control"
                                           placeholder="Suche nach Rechner Nr">
                                    <span class="input-group-addon">
                        <button type="submit">
                            <span class="glyphicon glyphicon-search"></span>
											</button>
											</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h3 class="sub-header">Rechner Übersicht</h3>


            <!-- Datenbank Tabellenkopf der Ausgabetabelle:-->
            <script src="../js/jquery.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>

            <div class="container-fluid">
                <div class="row">


                    <div class="col-md-12">
                        <div class="table-responsive">


                            <table id="mytable" class="table table-bordred table-striped table-hover">

                                <thead>

                                <th><input type="checkbox" id="checkall"/></th>
                                <th>Rechner Nr</th>
                                <th>Rechner Typ</th>
                                <th>OS Typ</th>
                                <th>HDD Größe</th>
                                <th>CPU Typ</th>
                                <th>RAM Größe</th>
                                <th>Display Größe</th>
                                <th>ODD</th>
                                <th>Siegel</th>
                                <?php if ($_SESSION['admin'] == 1) { //Spalte Helfer nur für Administratoren sichtbar ?>
                                    <th>Helfer</th>
                                <?php } ?>
                                <th></th>
                                </thead>


                                <?php

                                require_once('inc/db.php');


                                if (isset($_GET['aktion']) and $_GET['aktion'] == 'sicherheitsabfrage') {
                                    if (isset($_GET['id'])) {
                                        $id_einlesen = (INT)$_GET['id'];
                                        echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><b> Soll Datensatz ' . $id_einlesen . ' unwiderruflich gelöscht werden?</b></div><br>';
                                        echo '<a href="?aktion=loeschen&id=' . $id_einlesen . '" class="btn btn-danger" role="button">endgültig löschen</a>';
                                        echo '<b> oder </b>';
                                        echo '<a href="index.php" class="btn btn-warning" role="button">zurück</a>';
                                        echo '<hr>';
                                        $_GET['aktion'] = 'anzeigen';
                                    }
                                }

                                //Löschen eines Rechner-Datensatzes
                                if (isset($_GET['aktion']) and $_GET['aktion'] == 'loeschen') {
                                    if (isset($_GET['id'])) {
                                        $id = (INT)$_GET['id'];
                                        if ($id > 0) {
                                            $loeschen = $db->prepare("DELETE FROM rechner WHERE id=(?) LIMIT 1");
                                            $loeschen->bind_param('i', $id);
                                            if ($loeschen->execute()) {

                                                ?>
                                                <div class="alert alert-success alert-dismissable">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <strong>Datensatz <?php $id ?> wurde gelöscht!</strong>
                                                </div>

                                                <?php
                                            }
                                        }
                                    }
                                }

                                //Überprüfung der Formaulardaten, wenn ein Datensatz bearbeitet wurde
                                if (isset($_POST['aktion']) and $_POST['aktion'] == 'korrigieren') {
                                    $id = "";
                                    if (isset($_POST['Rechner_ID'])) {
                                        $id = (INT)trim(htmlspecialchars($_POST['Rechner_ID']));
                                    }
                                    $typ = "";
                                    if (isset($_POST['Rechner_Typ'])) {
                                        $typ = trim(htmlspecialchars($_POST['Rechner_Typ']));

                                    }
                                    $cpu = "";
                                    if (isset($_POST['Prozessor_Typ'])) {
                                        $cpu = trim(htmlspecialchars($_POST['Prozessor_Typ']));
                                    }
                                    $ram = "";
                                    if (isset($_POST['Arbeitsspeicher_Groesse'])) {
                                        $ram = trim(htmlspecialchars($_POST['Arbeitsspeicher_Groesse']));
                                    }
                                    $hdd = "";
                                    if (isset($_POST['Festplatten_Groesse'])) {
                                        $hdd = trim(htmlspecialchars($_POST['Festplatten_Groesse']));
                                    }
                                    $odd = "";
                                    if (isset($_POST['Optisches_Laufwerk'])) {
                                        $odd = trim(htmlspecialchars($_POST['Optisches_Laufwerk']));
                                    }
                                    $bildschirm = "";
                                    if (isset($_POST['Display_Groesse'])) {
                                        $bildschirm = trim(htmlspecialchars($_POST['Display_Groesse']));
                                    }
                                    $betriebssystem = "";
                                    if (isset($_POST['Betriebssystem_Typ'])) {
                                        $betriebssystem = trim(htmlspecialchars($_POST['Betriebssystem_Typ']));
                                    }
                                    $siegel = "";
                                    if (isset($_POST['Siegel'])) {
                                        $siegel = trim(htmlspecialchars($_POST['Siegel']));
                                    }
                                    if ($id != '' or $typ != '' or $cpu != '' or $ram != '' or $hdd != '' or $odd != '' or $bildschirm != '' or $betriebssystem != '' or $siegel != '') {
                                        // speichern der geänderten Werte
                                        $update = $db->prepare("UPDATE rechner SET 
                    typ=?, cpu=?, ram=?, hdd=?, odd=?, bildschirm=?, betriebssystem=?, siegel=? WHERE id=? LIMIT 1;");
                                        $update->bind_param('ssssssssi', $typ, $cpu, $ram, $hdd, $odd, $bildschirm, $betriebssystem, $siegel, $id);
                                        if ($update->execute()) {
                                            ?>
                                            <div class="alert alert-success alert-dismissable">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <strong>Datensatz <?php $id ?> wurde geändert!</strong>
                                            </div>

                                            <?php

                                        }
                                    }
                                }

                                //Formular zur erfassung und Darstellung der aktuellen Daten und Möglichkeit zu Änderung dieser
                                $modus_aendern = false;
                                if (isset($_GET['aktion']) and $_GET['aktion'] == 'bearbeiten') {
                                $modus_aendern = true;
                                if (isset($_GET['id'])) {
                                $id_einlesen = (INT)$_GET['id'];
                                if ($id_einlesen > 0)
                                {
                                $dseinlesen = $db->prepare("
            SELECT id, typ, cpu, ram, hdd, odd, bildschirm, betriebssystem, siegel FROM rechner WHERE id = ? ");
                                $dseinlesen->bind_param('i', $id_einlesen);
                                $dseinlesen->execute();
                                $dseinlesen->bind_result($id, $typ, $cpu, $ram, $hdd, $odd, $bildschirm, $betriebssystem, $siegel);
                                while ($dseinlesen->fetch()) {

                                ?>
                                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                                    <!-- start "Rechner" form -->
                                    <form class="form-horizontal" action="index.php" method="post">
                                        <fieldset>


                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Rechner_ID">Rechner
                                                    Nummer:</label>
                                                <div class="col-md-4">
                                                    <input id="Rechner_ID" name="Rechner_ID" placeholder="Rechner Nr"
                                                           class="form-control" required="" type="text"
                                                           value="<?php echo $id; ?>">

                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Rechner_Typ">Rechner
                                                    Typ:</label>
                                                <div class="col-md-4">
                                                    <select id="Rechner_Typ" name="Rechner_Typ" class="form-control">
                                                        <option value="Desktop" <?php if ($typ == "Desktop") {
                                                            echo "selected";
                                                        } ?>>Desktop
                                                        </option>
                                                        <option value="Laptop/Notebook" <?php if ($typ == "Laptop/Notebook") {
                                                            echo "selected";
                                                        } ?>>Laptop/Notebook
                                                        </option>
                                                        <option value="Netbook" <?php if ($typ == "Netbook") {
                                                            echo "selected";
                                                        } ?>>Netbook
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Betriebssystem_Typ">Betriebssystem
                                                    Typ:</label>
                                                <div class="col-md-4">
                                                    <select id="Betriebssystem_Typ" name="Betriebssystem_Typ"
                                                            class="form-control">
                                                        <option value="Ubuntu 14.04" <?php if ($betriebssystem == "Ubuntu 14.04") {
                                                            echo "selected";
                                                        } ?>>Ubuntu 14.04
                                                        </option>
                                                        <option value="Ubuntu 16.04" <?php if ($betriebssystem == "Ubuntu 16.04") {
                                                            echo "selected";
                                                        } ?>>Ubuntu 16.04
                                                        </option>
                                                        <option value="Lubuntu 14.04" <?php if ($betriebssystem == "Lubuntu 14.04") {
                                                            echo "selected";
                                                        } ?>>Lubuntu 14.04
                                                        </option>
                                                        <option value="Lubuntu 16.04" <?php if ($betriebssystem == "Lubuntu 16.04") {
                                                            echo "selected";
                                                        } ?>>Lubuntu 16.04
                                                        </option>
                                                        <option value="Windows Vista" <?php if ($betriebssystem == "Windows Vista") {
                                                            echo "selected";
                                                        } ?>>Windows Vista
                                                        </option>
                                                        <option value="Windows 7" <?php if ($betriebssystem == "Windows 7") {
                                                            echo "selected";
                                                        } ?>>Windows 7
                                                        </option>
                                                        <option value="Windows 8/8.1" <?php if ($betriebssystem == "Windows 8/8.1") {
                                                            echo "selected";
                                                        } ?>>Windows 8/8.1
                                                        </option>
                                                        <option value="Windows 10" <?php if ($betriebssystem == "Windows 10") {
                                                            echo "selected";
                                                        } ?>>Windows 10
                                                        </option>
                                                        <option value="Mac OS X" <?php if ($betriebssystem == "Mac OS X") {
                                                            echo "selected";
                                                        } ?>>Mac OS X
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Festplatten_Groesse">Festplatten
                                                    Größe:</label>
                                                <div class="col-md-4">
                                                    <select id="Festplatten_Groesse" name="Festplatten_Groesse"
                                                            class="form-control">
                                                        <option value="80gb" <?php if ($hdd == "80gb") {
                                                            echo "selected";
                                                        } ?>>80gb
                                                        </option>
                                                        <option value="160gb" <?php if ($hdd == "160gb") {
                                                            echo "selected";
                                                        } ?>>160gb
                                                        </option>
                                                        <option value="250gb" <?php if ($hdd == "250gb") {
                                                            echo "selected";
                                                        } ?>>250gb
                                                        </option>
                                                        <option value="320gb" <?php if ($hdd == "320gb") {
                                                            echo "selected";
                                                        } ?>>320gb
                                                        </option>
                                                        <option value="500gb" <?php if ($hdd == "500gb") {
                                                            echo "selected";
                                                        } ?>>500gb
                                                        </option>
                                                        <option value="750gb" <?php if ($hdd == "750gb") {
                                                            echo "selected";
                                                        } ?>>750gb
                                                        </option>
                                                        <option value="1000gb" <?php if ($hdd == "1000gb") {
                                                            echo "selected";
                                                        } ?>>1000gb
                                                        </option>
                                                        <option value="2000gb" <?php if ($hdd == "2000gb") {
                                                            echo "selected";
                                                        } ?>>2000gb
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Prozessor_Typ">Prozessor
                                                    Typ:</label>
                                                <div class="col-md-4">
                                                    <select id="Prozessor_Typ" name="Prozessor_Typ"
                                                            class="form-control">
                                                        <option value="AMD-FX" <?php if ($cpu == "AMD-FX") {
                                                            echo "selected";
                                                        } ?>>AMD-FX
                                                        </option>
                                                        <option value="AMD-Phenom" <?php if ($cpu == "AMD-Phenom") {
                                                            echo "selected";
                                                        } ?>>AMD-Phenom
                                                        </option>
                                                        <option value="AMD-Athlon" <?php if ($cpu == "AMD-Athlon") {
                                                            echo "selected";
                                                        } ?>>AMD-Athlon
                                                        </option>
                                                        <option value="AMD-Sempron" <?php if ($cpu == "AMD-Sempron") {
                                                            echo "selected";
                                                        } ?>>AMD-Sempron
                                                        </option>
                                                        <option value="INTEL-Atom" <?php if ($cpu == "INTEL-Atom") {
                                                            echo "selected";
                                                        } ?>>INTEL-Atom
                                                        </option>
                                                        <option value="INTEL-Pentium" <?php if ($cpu == "INTEL-Pentium") {
                                                            echo "selected";
                                                        } ?>>INTEL-Pentium
                                                        </option>
                                                        <option value="INTEL-Centrino" <?php if ($cpu == "INTEL-Centrino") {
                                                            echo "selected";
                                                        } ?>>INTEL-Centrino
                                                        </option>
                                                        <option value="INTEL-Celeron" <?php if ($cpu == "INTEL-Celeron") {
                                                            echo "selected";
                                                        } ?>>INTEL-Celeron
                                                        </option>
                                                        <option value="INTEL-CoreDuo" <?php if ($cpu == "INTEL-CoreDuo") {
                                                            echo "selected";
                                                        } ?>>INTEL-CoreDuo
                                                        </option>
                                                        <option value="INTEL-Corei" <?php if ($cpu == "INTEL-Corei") {
                                                            echo "selected";
                                                        } ?>>INTEL-Corei
                                                        </option>
                                                        <option value="PowerPC" <?php if ($cpu == "PowerPC") {
                                                            echo "selected";
                                                        } ?>>PowerPC
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Arbeitsspeicher_Groesse">Arbeitsspeicher
                                                    Größe:</label>
                                                <div class="col-md-4">
                                                    <select id="Arbeitsspeicher_Groesse" name="Arbeitsspeicher_Groesse"
                                                            class="form-control">
                                                        <option value="512mb" <?php if ($ram == "512mb") {
                                                            echo "selected";
                                                        } ?>>512mb
                                                        </option>
                                                        <option value="1024mb" <?php if ($ram == "1024mb") {
                                                            echo "selected";
                                                        } ?>>1024mb
                                                        </option>
                                                        <option value="2048mb" <?php if ($ram == "2048mb") {
                                                            echo "selected";
                                                        } ?>>2048mb
                                                        </option>
                                                        <option value="3072mb" <?php if ($ram == "3072mb") {
                                                            echo "selected";
                                                        } ?>>3072mb
                                                        </option>
                                                        <option value="4096mb" <?php if ($ram == "4096mb") {
                                                            echo "selected";
                                                        } ?>>4096mb
                                                        </option>
                                                        <option value="6144mb" <?php if ($ram == "6144mb") {
                                                            echo "selected";
                                                        } ?>>6144mb
                                                        </option>
                                                        <option value="8192mb" <?php if ($ram == "8192mb") {
                                                            echo "selected";
                                                        } ?>>8192mb
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Select Basic -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Display_Groesse">Display
                                                    Größe:</label>
                                                <div class="col-md-4">
                                                    <select id="Display_Groesse" name="Display_Groesse"
                                                            class="form-control">
                                                        <option value="10_Zoll" <?php if ($bildschirm == "10_Zoll") {
                                                            echo "selected";
                                                        } ?>>10_Zoll
                                                        </option>
                                                        <option value="11_Zoll" <?php if ($bildschirm == "11_Zoll") {
                                                            echo "selected";
                                                        } ?>>11_Zoll
                                                        </option>
                                                        <option value="12_Zoll" <?php if ($bildschirm == "12_Zoll") {
                                                            echo "selected";
                                                        } ?>>12_Zoll
                                                        </option>
                                                        <option value="13_Zoll" <?php if ($bildschirm == "13_Zoll") {
                                                            echo "selected";
                                                        } ?>>13_Zoll
                                                        </option>
                                                        <option value="14_Zoll" <?php if ($bildschirm == "14_Zoll") {
                                                            echo "selected";
                                                        } ?>>14_Zoll
                                                        </option>
                                                        <option value="15_Zoll" <?php if ($bildschirm == "15_Zoll") {
                                                            echo "selected";
                                                        } ?>>15_Zoll
                                                        </option>
                                                        <option value="16_Zoll" <?php if ($bildschirm == "16_Zoll") {
                                                            echo "selected";
                                                        } ?>>16_Zoll
                                                        </option>
                                                        <option value="17_Zoll" <?php if ($bildschirm == "17_Zoll") {
                                                            echo "selected";
                                                        } ?>>17_Zoll
                                                        </option>
                                                        <option value="18_Zoll" <?php if ($bildschirm == "18_Zoll") {
                                                            echo "selected";
                                                        } ?>>18_Zoll
                                                        </option>
                                                        <option value="19_Zoll" <?php if ($bildschirm == "19_Zoll") {
                                                            echo "selected";
                                                        } ?>>19_Zoll
                                                        </option>
                                                        <option value="20_Zoll" <?php if ($bildschirm == "20_Zoll") {
                                                            echo "selected";
                                                        } ?>>20_Zoll
                                                        </option>
                                                        <option value="21_Zoll" <?php if ($bildschirm == "21_Zoll") {
                                                            echo "selected";
                                                        } ?>>21_Zoll
                                                        </option>
                                                        <option value="22_Zoll" <?php if ($bildschirm == "22_Zoll") {
                                                            echo "selected";
                                                        } ?>>22_Zoll
                                                        </option>
                                                        <option value="23_Zoll" <?php if ($bildschirm == "23_Zoll") {
                                                            echo "selected";
                                                        } ?>>23_Zoll
                                                        </option>
                                                        <option value="24_Zoll" <?php if ($bildschirm == "24_Zoll") {
                                                            echo "selected";
                                                        } ?>>24_Zoll
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>


                                            <!-- Multiple Radios (inline) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Optisches_Laufwerk">Optisches
                                                    Laufwerk:</label>
                                                <div class="col-md-4">
                                                    <label class="radio-inline" for="Optisches_Laufwerk-0">
                                                        <input name="Optisches_Laufwerk" id="Optisches_Laufwerk-0"
                                                               value="ja" <?php if ($odd == "Ja" OR "ja") {
                                                            echo 'checked=""';
                                                        } ?> type="radio">
                                                        vorhanden
                                                    </label>
                                                    <label class="radio-inline" for="Optisches_Laufwerk-1">
                                                        <input name="Optisches_Laufwerk" id="Optisches_Laufwerk-1"
                                                               value="nein" <?php if ($odd == "Nein" OR "nein") {
                                                            echo 'checked=""';
                                                        } ?> type="radio">
                                                        nicht vorhanden
                                                    </label>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Multiple Radios (inline) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Siegel">Siegel
                                                    vorhanden:</label>
                                                <div class="col-md-4">
                                                    <label class="radio-inline" for="Siegel-0">
                                                        <input name="Siegel" id="Siegel-0"
                                                               value="ja" <?php if ($siegel == "Ja" OR "ja") {
                                                            echo 'checked=""';
                                                        } ?> type="radio">
                                                        ja
                                                    </label>
                                                    <label class="radio-inline" for="Siegel-1">
                                                        <input name="Siegel" id="Siegel-1"
                                                               value="nein" <?php if ($siegel == "Nein" OR "nein") {
                                                            echo 'checked=""';
                                                        } ?> type="radio">
                                                        nein
                                                    </label>
                                                </div>
                                            </div>
                                            <br><br>

                                            <!-- Button (Double) -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="Reset"></label>
                                                <div class="col-md-8">

                                                    <a href="index.php" class="btn btn-danger">Änderungen
                                                        zurücksetzen</a>
                                                    <input type="hidden" name="id" value="<?php $id ?>">
                                                    <input type="hidden" name="aktion" value="korrigieren">
                                                    <button type="submit" class="btn btn-primary">Änderungen speichern
                                                    </button>


                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                    <hr>
                                    <?php
                                    }
                                    }
                                    }
                                    }


                                    if (isset($_GET['anzeigen'])) {
                                        $anzeigen = $_GET['anzeigen'];
                                    } else {
                                        $anzeigen = "n";
                                    }

                                    //Ausgabe der Daten als Tabelle
                                    //Zusätzlich Unterscheidung wie viele Datensätze aus der Tabelle geladen werden, dazu Link am Tabellenende.
                                    if ($anzeigen === 'j') {
                                        $erg = $db->query("SELECT id, typ, betriebssystem, hdd, cpu, ram, bildschirm, odd, siegel, mitarbeiter FROM rechner ORDER BY id DESC")
                                        or die($db->error);
                                    } else {
                                        $erg = $db->query("SELECT id, typ, betriebssystem, hdd, cpu, ram, bildschirm, odd, siegel, mitarbeiter FROM rechner ORDER BY id DESC LIMIT 40")
                                        or die($db->error);
                                    }
                                    // while ($zeile = $erg->fetch_assoc()) {
                                    while ($zeile = $erg->fetch_object()) {
                                        ?>

                                        <tr>
                                            <td><input type='checkbox' class='checkthis'/></td>
                                            <td>
                                                <?php echo $zeile->id; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->typ; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->betriebssystem; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->hdd; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->cpu; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->ram; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->bildschirm; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->odd; ?>
                                            </td>
                                            <td>
                                                <?php echo $zeile->siegel; ?>
                                            </td>
                                            <?php if ($_SESSION['admin'] == 1) { ?>
                                                <td>
                                                    <?php echo $zeile->mitarbeiter; ?>
                                                </td>
                                            <?php } ?>
                                            <td><a href="?aktion=bearbeiten&id=<?php echo $zeile->id; ?>"
                                                   class='btn btn-primary btn-xs' data-title='Delete'><span
                                                            class='glyphicon glyphicon-pencil'></span></a></p>
                                            </td>
                                            <td><a href="?aktion=sicherheitsabfrage&id=<?php echo $zeile->id; ?>"
                                                   class='btn btn-danger btn-xs' data-title='Delete'><span
                                                            class='glyphicon glyphicon-trash'></span></a></td>
                                        </tr>
                                        <?php
                                    }
                                    echo "</table>";

                                    $erg->free();
                                    $db->close();

                                    //Bestimmt ob alle Datensätze aus dieser Tabelle geladen werden sollen.
                                    if (!isset($_GET['anzeigen'])) {
                                        echo "<a href='?anzeigen=j'>Alle Datensätze anzeigen</a>";
                                    }
                                    ?>


                                </div>
                        </div>
                    </div>
                </div>

                <!-- Formular, um neue Computer in die Datenbank einzutragen-->
                <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                            class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Datensatz bearbeiten:</h4>
                            </div>
                            <div class="modal-body">

                                <form class="form-horizontal" action="ausgabe.php" method="post">
                                    <fieldset>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Rechner_ID">Rechner
                                                Nummer:</label>
                                            <div class="col-md-4">
                                                <input id="Rechner_ID" name="Rechner_ID" placeholder=""
                                                       class="form-control" required="" type="text">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Rechner_Typ">Rechner Typ:</label>
                                            <div class="col-md-4">
                                                <select id="Rechner_Typ" name="Rechner_Typ" class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="Desktop">Desktop</option>
                                                    <option value="Laptop/Notebook">Laptop/Notebook</option>
                                                    <option value="Netbook">Netbook</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Betriebssystem_Typ">Betriebssystem
                                                Typ:</label>
                                            <div class="col-md-4">
                                                <select id="Betriebssystem_Typ" name="Betriebssystem_Typ"
                                                        class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="Ubuntu 14.04">Ubuntu 14.04</option>
                                                    <option value="Ubuntu 16.04">Ubuntu 16.04</option>
                                                    <option value="Lubuntu 14.04">Lubuntu 14.04</option>
                                                    <option value="Lubuntu 16.04">Lubuntu 16.04</option>
                                                    <option value="Windows Vista">Windows Vista</option>
                                                    <option value="Windows 7">Windows 7</option>
                                                    <option value="Windows 8/8.1">Windows 8/8.1</option>
                                                    <option value="Windows 10">Windows 10</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Festplatten_Groesse">Festplatten
                                                Größe:</label>
                                            <div class="col-md-4">
                                                <select id="Festplatten_Groesse" name="Festplatten_Groesse"
                                                        class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="80gb">80gb</option>
                                                    <option value="160gb">160gb</option>
                                                    <option value="250gb">250gb</option>
                                                    <option value="320gb">320gb</option>
                                                    <option value="500gb">500gb</option>
                                                    <option value="750gb">750gb</option>
                                                    <option value="1000gb">1000gb</option>
                                                    <option value="2000gb">2000gb</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Prozessor_Typ">Prozessor
                                                Typ:</label>
                                            <div class="col-md-4">
                                                <select id="Prozessor_Typ" name="Prozessor_Typ" class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="AMD-FX">AMD-FX</option>
                                                    <option value="AMD-Phenom">AMD-Phenom</option>
                                                    <option value="AMD-Athlon">AMD-Athlon</option>
                                                    <option value="AMD-Sempron">AMD-Sempron</option>
                                                    <option value="INTEL-Atom">INTEL-Atom</option>
                                                    <option value="INTEL-Pentium">INTEL-Pentium</option>
                                                    <option value="INTEL-Celeron">INTEL-Celeron</option>
                                                    <option value="INTEL-CoreDuo">INTEL-CoreDuo</option>
                                                    <option value="INTEL-Corei">INTEL-Corei</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Arbeitsspeicher_Groesse">Arbeitsspeicher
                                                Größe:</label>
                                            <div class="col-md-4">
                                                <select id="Arbeitsspeicher_Groesse" name="Arbeitsspeicher_Groesse"
                                                        class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="512mb">512mb</option>
                                                    <option value="1024mb">1024mb</option>
                                                    <option value="2048mb">2048mb</option>
                                                    <option value="3072mb">3072mb</option>
                                                    <option value="4096mb">4096mb</option>
                                                    <option value="6144mb">6144mb</option>
                                                    <option value="8192mb">8192mb</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Display_Groesse">Display
                                                Größe:</label>
                                            <div class="col-md-4">
                                                <select id="Display_Groesse" name="Display_Groesse"
                                                        class="form-control">
                                                    <option value="" selected disabled>Bitte wählen:</option>
                                                    <option value="10_Zoll">10_Zoll</option>
                                                    <option value="11_Zoll">11_Zoll</option>
                                                    <option value="12_Zoll">12_Zoll</option>
                                                    <option value="13_Zoll">13_Zoll</option>
                                                    <option value="14_Zoll">14_Zoll</option>
                                                    <option value="15_Zoll">15_Zoll</option>
                                                    <option value="16_Zoll">16_Zoll</option>
                                                    <option value="17_Zoll">17_Zoll</option>
                                                    <option value="18_Zoll">18_Zoll</option>
                                                    <option value="19_Zoll">19_Zoll</option>
                                                    <option value="20_Zoll">20_Zoll</option>
                                                    <option value="21_Zoll">21_Zoll</option>
                                                    <option value="22_Zoll">22_Zoll</option>
                                                    <option value="23_Zoll">23_Zoll</option>
                                                    <option value="24_Zoll">24_Zoll</option>
                                                </select>
                                            </div>
                                        </div>
                            </div>
                            <div class="modal-footer ">
                                <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span
                                            class="glyphicon glyphicon-ok-sign"></span> Datensatz speichern
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


                <!--<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Datensatz löschen</h4>
                        </div>
                        <div class="modal-body">

                            <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Bis du dir sicher, dass du diesen Datensatz löschen möchtest?</div>

                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> Ja</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Nein</button>
                        </div>
                    </div>-->
                <!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>
</body>

</html>