<?php

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="../mainpage/index.php">Übersicht<span class="sr-only">(current)</span></a></li>
                <li class="active"><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href=../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li><a href="../rechner_db/index.php">Rechner Übersicht</a></li>
                <li><a href="../rechner_form/index.php">Rechner eintragen</a></li>
                <li class="active"><a href="">Datensatz hinzugefügt</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h3 class="page-header">Datensatz erfolgreich hinzugefügt!</h3>
            <div class="text-center">
                <h3>Weitere Aktionen:</h3><br/>
                <div class="center-block">
                    <a href="../rechner_db/index.php"
                    <button type="button" class="btn btn-primary btn-md">zurück zur Übersicht</button>
                    </a>
                    <a href="../login-page/logout.php"
                    <button type="button" class="btn btn-warning btn-md">Logout</button>
                    </a>
                    <br/>
                    <br/><h4><b>oder:</b></h4> <br/>
                    <a href="index.php"
                    <button type="button" class="btn btn-primary btn-md">weiteren Rechner eintragen</button>
                    </a>
                </div>
            </div>
            <br/>
            <hr/>
            <br/>

            <?php
            //Auswertung der Daten aus dem rechner_form/index.php Formular
            require_once('../rechner_db/inc/db.php');

            $Rechner_ID = (isset($_POST["Rechner_ID"]) AND is_numeric($_POST["Rechner_ID"]) AND $_POST["Rechner_ID"] > 0 AND $_POST["Rechner_ID"] < 9999) ? htmlspecialchars($_POST["Rechner_ID"]) : "error";
            $Rechner_Typ = (isset($_POST["Rechner_Typ"])) ? htmlspecialchars($_POST["Rechner_Typ"]) : "";
            $Betriebssystem_Typ = (isset($_POST["Betriebssystem_Typ"])) ? htmlspecialchars($_POST["Betriebssystem_Typ"]) : "";
            $Festplatten_Groesse = (isset($_POST["Festplatten_Groesse"])) ? htmlspecialchars($_POST["Festplatten_Groesse"]) : "";
            $Prozessor_Typ = (isset($_POST["Prozessor_Typ"])) ? htmlspecialchars($_POST["Prozessor_Typ"]) : "";
            $Arbeitsspeicher_Groesse = (isset($_POST["Arbeitsspeicher_Groesse"])) ? htmlspecialchars($_POST["Arbeitsspeicher_Groesse"]) : "";
            $Display_Groesse = (isset($_POST["Display_Groesse"])) ? htmlspecialchars($_POST["Display_Groesse"]) : "";
            $Optisches_Laufwerk = (isset($_POST["Optisches_Laufwerk"])) ? htmlspecialchars($_POST["Optisches_Laufwerk"]) : "";
            $Siegel = (isset($_POST["Siegel"])) ? htmlspecialchars($_POST["Siegel"]) : "";
            $Mitarbeiter = strstr($user['email'], "@", true);

            if ($Rechner_ID == "error") {
                echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><b> Datensatz konnte aufgrund eines Fehlers nicht eingetragen werden. Bitte veruche es erneut, achte ggf. auf die korrekte Schreibweise der ID (1234 statt 01234).</b></div><br>';
            }
            //Abspeichern des neuen Rechner-Datensatzes
            if (isset($db) AND $Rechner_ID != "error") {
                $sql = "INSERT INTO rechner
               (id,
			   typ,
               betriebssystem,
               hdd,
               cpu,
               ram,
               bildschirm,
               odd,
               siegel,
               mitarbeiter)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $kommando = mysqli_prepare($db, $sql);
                mysqli_stmt_bind_param($kommando, "isssssssss",
                    $Rechner_ID,
                    $Rechner_Typ,
                    $Betriebssystem_Typ,
                    $Festplatten_Groesse,
                    $Prozessor_Typ,
                    $Arbeitsspeicher_Groesse,
                    $Display_Groesse,
                    $Optisches_Laufwerk,
                    $Siegel,
                    $Mitarbeiter);
                if (mysqli_stmt_execute($kommando)) {
                    $id = mysqli_insert_id($db);
                    #echo "Eintrag hinzugef&uuml;gt.
                    #<a href=\"gb-edit.php?id=$id\">Bearbeiten</a>";
                } else {
                    //echo "Fehler: " . mysqli_error($db) . "!";
                }
                mysqli_close($db);
            } else {
                //echo "Fehler: " . mysqli_connect_error() . "!";
            }

            ?>


            <!-- Übersicht welche Daten neu eingetragen werden -->
            <h4 class="sub-header">Folgende Daten wurden eingetragen/modifiziert:</h4>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Rechner_Nr</th>
                        <th>Rechner_Typ</th>
                        <th>OS_Typ</th>
                        <th>HDD_Größe</th>
                        <th>CPU_Typ</th>
                        <th>RAM_Größe</th>
                        <th>Display_Größe</th>
                        <th>CD/DVD</th>
                        <th>Siegel</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $Rechner_ID ?></td>
                        <td><?php echo $Rechner_Typ ?></td>
                        <td><?php echo $Betriebssystem_Typ ?></td>
                        <td><?php echo $Festplatten_Groesse ?></td>
                        <td><?php echo $Prozessor_Typ ?></td>
                        <td><?php echo $Arbeitsspeicher_Groesse ?></td>
                        <td><?php echo $Display_Groesse ?></td>
                        <td><?php echo $Optisches_Laufwerk ?></td>
                        <td><?php echo $Siegel ?></td>
                    </tr>

                    </tbody>
                </table>
            </div>

            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
            <script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>
