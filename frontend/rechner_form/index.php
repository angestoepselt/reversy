<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a></li>
                <li class="active"><a href="#">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li><a href="../rechner_db/index.php">Rechner Übersicht</a></li>
                <li class="active"><a href="">Rechner eintragen</a></li>
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- start "Rechner" form -->
            <form class="form-horizontal" action="ausgabe.php" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Rechner-Datensatz einfügen</legend>

                    <div class="alert alert-warning">
                        <span class="glyphicon glyphicon-warning-sign"></span> Bitte überprüfe vor dem Eintragen des
                        Rechners, ob alle benötigten Updates installiert sind.<p>Bei geclonten Computern sind die
                            Updates meistens schon installiert.</p>
                        <p>Falls Ubuntu noch geupdatet werden muss kannst du dises Skript dafür verwenden:</p>
                        <p>1. Lade dir das Skript <a href="reversy-update.sh">HIER</a> herunter</p>
                        <p>2. Navigiere im Terminal in den Downloads Ordner <code>cd Downloads/</code></p>
                        <p>3. Mache das Skript ausführbar <code>chmod +x reversy-update.sh</code></p>
                        <p>4. Führe nun das Skript aus <code>./reversy-update.sh</code></p>

                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Rechner_ID">Rechner Nummer:</label>
                        <div class="col-md-4">
                            <input id="Rechner_ID" name="Rechner_ID" placeholder="Rechner Nr" class="form-control"
                                   required="" type="text">

                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Rechner_Typ">Rechner Typ:</label>
                        <div class="col-md-4">
                            <select id="Rechner_Typ" name="Rechner_Typ" class="form-control" required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="Desktop">Desktop</option>
                                <option value="Laptop/Notebook">Laptop/Notebook</option>
                                <option value="Netbook">Netbook</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Betriebssystem_Typ">Betriebssystem Typ:</label>
                        <div class="col-md-4">
                            <select id="Betriebssystem_Typ" name="Betriebssystem_Typ" class="form-control" required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="Ubuntu 14.04">Ubuntu 14.04</option>
                                <option value="Ubuntu 16.04">Ubuntu 16.04</option>
                                <option value="Lubuntu 14.04">Lubuntu 14.04</option>
                                <option value="Lubuntu 16.04">Lubuntu 16.04</option>
                                <option value="Windows Vista">Windows Vista</option>
                                <option value="Windows 7">Windows 7</option>
                                <option value="Windows 8/8.1">Windows 8/8.1</option>
                                <option value="Windows 10">Windows 10</option>
                                <option value="Mac OS X">Mac OS X</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Festplatten_Groesse">Festplatten Größe:</label>
                        <div class="col-md-4">
                            <select id="Festplatten_Groesse" name="Festplatten_Groesse" class="form-control"
                                    required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="80gb">80gb</option>
                                <option value="160gb">160gb</option>
                                <option value="250gb">250gb</option>
                                <option value="320gb">320gb</option>
                                <option value="500gb">500gb</option>
                                <option value="750gb">750gb</option>
                                <option value="1000gb">1000gb</option>
                                <option value="2000gb">2000gb</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Prozessor_Typ">Prozessor Typ:</label>
                        <div class="col-md-4">
                            <select id="Prozessor_Typ" name="Prozessor_Typ" class="form-control" required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="AMD-FX">AMD-FX</option>
                                <option value="AMD-Phenom">AMD-Phenom</option>
                                <option value="AMD-Athlon">AMD-Athlon</option>
                                <option value="AMD-Sempron">AMD-Sempron</option>
                                <option value="INTEL-Atom">INTEL-Atom</option>
                                <option value="INTEL-Pentium">INTEL-Pentium</option>
                                <option value="INTEL-Centrino">INTEL-Centrino</option>
                                <option value="INTEL-Celeron">INTEL-Celeron</option>
                                <option value="INTEL-CoreDuo">INTEL-CoreDuo</option>
                                <option value="INTEL-Corei">INTEL-Corei</option>
                                <option value="PowerPC">PowerPC</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Arbeitsspeicher_Groesse">Arbeitsspeicher
                            Größe:</label>
                        <div class="col-md-4">
                            <select id="Arbeitsspeicher_Groesse" name="Arbeitsspeicher_Groesse" class="form-control"
                                    required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="512mb">512mb</option>
                                <option value="1024mb">1024mb</option>
                                <option value="2048mb">2048mb</option>
                                <option value="3072mb">3072mb</option>
                                <option value="4096mb">4096mb</option>
                                <option value="6144mb">6144mb</option>
                                <option value="8192mb">8192mb</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Display_Groesse">Display Größe:</label>
                        <div class="col-md-4">
                            <select id="Display_Groesse" name="Display_Groesse" class="form-control" required="">
                                <option value="" selected disabled>Bitte wählen:</option>
                                <option value="10_Zoll">10_Zoll</option>
                                <option value="11_Zoll">11_Zoll</option>
                                <option value="12_Zoll">12_Zoll</option>
                                <option value="13_Zoll">13_Zoll</option>
                                <option value="14_Zoll">14_Zoll</option>
                                <option value="15_Zoll">15_Zoll</option>
                                <option value="16_Zoll">16_Zoll</option>
                                <option value="17_Zoll">17_Zoll</option>
                                <option value="18_Zoll">18_Zoll</option>
                                <option value="19_Zoll">19_Zoll</option>
                                <option value="20_Zoll">20_Zoll</option>
                                <option value="21_Zoll">21_Zoll</option>
                                <option value="22_Zoll">22_Zoll</option>
                                <option value="23_Zoll">23_Zoll</option>
                                <option value="24_Zoll">24_Zoll</option>
                            </select>
                        </div>
                    </div>


                    <!-- Multiple Radios (inline) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Optisches_Laufwerk">Optisches Laufwerk:</label>
                        <div class="col-md-4">
                            <label class="radio-inline" for="Optisches_Laufwerk-0">
                                <input name="Optisches_Laufwerk" id="Optisches_Laufwerk-0" value="ja" checked="checked"
                                       type="radio">
                                vorhanden
                            </label>
                            <label class="radio-inline" for="Optisches_Laufwerk-1">
                                <input name="Optisches_Laufwerk" id="Optisches_Laufwerk-1" value="nein" type="radio">
                                nicht vorhanden
                            </label>
                        </div>
                    </div>

                    <!-- Multiple Radios (inline) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Siegel">Siegel vorhanden:</label>
                        <div class="col-md-4">
                            <label class="radio-inline" for="Siegel-0">
                                <input name="Siegel" id="Siegel-0" value="ja" type="radio">
                                ja
                            </label>
                            <label class="radio-inline" for="Siegel-1">
                                <input name="Siegel" id="Siegel-1" value="nein" checked="checked" type="radio">
                                nein
                            </label>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Reset"></label>
                        <div class="col-md-8">
                            <button id="Reset" type="reset" name="Reset" class="btn btn-danger">Eingaben zurücksetzen
                            </button>
                            <button id="Submit" type="submit" name="Submit" class="btn btn-primary">Rechner eintragen
                            </button>
                        </div>
                    </div>

                </fieldset>
            </form>
            </br>
            </br>
            </br>


            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
            <script src="../../dist/js/bootstrap.min.js"></script>

</body>
</html>
