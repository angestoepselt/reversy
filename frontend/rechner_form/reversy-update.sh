#!/bin/sh

#Angestöpselt reversy Update Skript
#Automatische Ubuntu Updates über einen lokalen/externen Spiegelserver



if ! zenity --question --text "Willkommen zum reversy Update Skript bitte bestätige, dass du alle Updates installieren möchtest." --title "reversy Update" 2> /dev/null ; then
  exit;
fi

#Root Passwortabfrage
PASSW=$(zenity --password "Bitte gib hier das root (sudo) Passwort ein:" --title "reversy Update")
echo "$PASSW" | sudo -s -S
PASSW=""

#Ersetzen der öffentlichen mirror in der sources.list durch private Speigelserver.
#Danach Update aller Pakete sowie zurücksetzten der sources.list.
(
echo "10" ; sudo cp /etc/apt/sources.list /tmp/ &&  sudo sed -i s/de.archive.ubuntu.com/csw-spiegelserver:9999/g /etc/apt/sources.list && sudo sed -i s/archive.canonical.com/csw-spiegelserver:9999/g /etc/apt/sources.list && sudo sed -i s/extras.ubuntu.com/csw-spiegelserver:9999/g /etc/apt/sources.list
echo "# Modifizieren der Quellendatei" ; sleep 1
echo "20" ; sudo apt-get update
echo "# Update der Paketlisten" ; sleep 1
echo "50" ; sudo apt-get -y upgrade
echo "# Holen der Updates" ; sleep 1
echo "75" ; sudo apt-get -y dist-upgrade
echo "# Zurücksetzten der Quellendatei" ; sleep 1
echo "80" ; sudo rm /etc/apt/sources.list && sudo mv /tmp/sources.list /etc/apt/
echo "# Fertig!" ; sleep 1
echo "100" ; sleep 1
) |
zenity --progress \
  --title="reversy Update" \
  --text="Breite auf Updates vor.." \
  --percentage=0

if [ "$?" = -1 ] ; then
        zenity --error \
          --text="Update canceled."
fi

#Möglichkeit Libre Office zu installieren/updaten
if ! zenity --question --text "Soll zusätzlich noch Libre-Office installiert/geupdated werden? (Bei Lubuntu empfohlen)" --title "reversy Update" 2> /dev/null ; then
  zenity --info \
  --text="Alle Updates/Änderungen wurden erfolgreich installiert, bitte starte den Computer neu!" --title "reversy Update"
  exit
fi

#Libre Office installation (falls ausgewählt)
(
echo "# Libre-Office wird installiert..." ; sleep 1
echo "100" ; sudo apt-get install -y libreoffice libreoffice-l10n-de libreoffice-help-de
) |
zenity --progress \
  --title="reversy Update" \
  --text="Installation von Libreoffice" \
  --percentage=0

if [ "$?" = -1 ] ; then
        zenity --error \
          --text="Update canceled."
fi

zenity --info \
--text="Alle Updates/Änderungen wurden erfolgreich installiert, bitte starte den Computer neu!" --title "reversy Update"

sudo -k
