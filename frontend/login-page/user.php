<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}

require_once('../rechner_db/inc/db.php');

?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/counter.css" rel="stylesheet">


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href=><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>

                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Übersicht <span class="sr-only">(current)</span></a></li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li><a href="../pdf_docs/delete_confirm_form.php">Rechner Löschbestätigung</a></li>
                <li><a href="../pdf_docs/auslagen_form.php">Auslagen Erstattung</a></li>
                <!--<li><a href="../mail/mail.php">Status Mailer</a></li>-->
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h2 class="page-header">Profil</h2>
            <form class="form-horizontal" method="POST" action="">
                <fieldset>

                    <!-- Übersicht über die Einstellungen des jeweiligen Benutzers mit Möglichkeit das eigene Passwort zu ändern. -->
                    <legend>Benutzereinstellungen:</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Email">Email:</label>
                        <div class="col-md-4">
                            <input id="Email" name="Email" placeholder="<?php $email = $user['email'];
                            echo $email; ?>" class="form-control input-md" disabled type="text">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="benutzername">Benutzername:</label>
                        <div class="col-md-4">
                            <input id="benutzername" name="benutzername"
                                   placeholder="<?php $extension = ucfirst(strstr($user['email'], "@", true));
                                   echo $extension; ?>" class="form-control input-md" disabled type="text">
                            <span class="help-block">Wir automatisch aus deiner Email generiert.</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="group">Benutzergruppe:</label>
                        <div class="col-md-4">
                            <input id="group" name="group" placeholder="<?php if ($_SESSION['admin'] == 1) {
                                echo "Administrator";
                            } else {
                                echo "User";
                            } ?>" class="form-control input-md" disabled type="text">

                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="pw1">Passwort neu setzen:</label>
                        <div class="col-md-4">
                            <input id="pw1" name="pw1" placeholder="Bitte wähle ein sicheres Passwort!"
                                   class="form-control input-md" type="password">

                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="pw2"></label>
                        <div class="col-md-4">
                            <input id="pw2" name="pw2" placeholder="Bitte wähle ein sicheres Passwort!"
                                   class="form-control input-md" type="password">
                            <span class="help-block">Passwort erneut bestätigen.</span>
                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="reset"></label>
                        <div class="col-md-8">
                            <button id="reset" name="reset" class="btn btn-danger">Eingaben zurücksetzen</button>
                            <button id="submit" name="submit" class="btn btn-primary">Einstellungen ändern</button>
                            </br></br>
                            <?php
                            //Überprüfung und validierung der Eingaben (Passwort) des Benutzers
                            //Achtung das Passwort kann frei gewählt werden, es gibt keine Mindestanforderungen
                            if (isset($_POST['pw1'])) {
                                $pw1 = (isset($_POST["pw1"])) ? $_POST["pw1"] : "";
                                $pw2 = (isset($_POST["pw2"])) ? $_POST["pw2"] : "";
                                if ($pw1 != $pw2) {
                                    echo "Achtung, Passwörter stimmen nicht überein!";
                                }

                                if ($pw1 == $pw2) {

                                    //Verschlüsselung und abspeichern des neuen Passwordes
                                    $email = $user['email'];
                                    $new_password = password_hash($pw1, PASSWORD_BCRYPT);

                                    if ($pw1 === $pw2) {
                                        $sql = "UPDATE users SET password = '$new_password' WHERE email = '$email'";
                                        $stmt = $conn->prepare($sql);
                                        $stmt->execute();
                                        echo "Das Passwort wurde erfolgreich aktualisiert!";


                                    }

                                }
                            }
                            ?>
                        </div>
                    </div>

                </fieldset>
            </form>

            <hr>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>
</body>

</html>