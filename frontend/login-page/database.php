<?php
//Logindaten der Datenbank im PDO Format weitere Logindaten in rechner_db/inc/db.php
require_once(__DIR__ . '/../../config.php');


try {
    $conn = new PDO("mysql:host=$cserver;dbname=$cdatabase;", $cuser, $cpw);
} catch (PDOException $e) {
    die("Connection failed: " . $e->getMessage());
}
?>