<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}


?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php"> Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li><a href="#"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="../login-page/user.php">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a></li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li class="active"><a href="../antrag_db/index.php">Kunden-DB</a></li>

            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li><a href="../antrag_db/index.php">Kunden Übersicht</a></li>
                <li class="active"><a href="">Kunden eintragen</a></li>
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- start "Rechner" form -->
            <form class="form-horizontal" action="ausgabe.php" method="post" enctype="multipart/form-data"
                  accept-charset="utf-8">
                <fieldset>

                    <!-- Formular zum Eintragen der Kunden in die Datenbank. Weitere Behandlung der Daten erfolgt in ausgabe.php -->

                    <legend>Kunden-Datensatz einfügen</legend>
                    <form class="form-horizontal">
                        <fieldset>

                            <form class="form-horizontal">
                                <fieldset>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Via">Via:</label>
                                        <div class="col-md-4">
                                            <select id="Via" name="Via" class="form-control" required="">
                                                <option value="Pers">Pers</option>
                                                <option value="Brief">Brief</option>
                                                <option value="Mail">Email</option>
                                                <option value="Tel">Tel</option>
                                                <option value="Ab/Fax">Ab/Fax</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Nachweis_Typ">Nachweis Typ:</label>
                                        <div class="col-md-4">
                                            <select id="Nachweis_Typ" name="Nachweis_Typ" class="form-control"
                                                    required="">
                                                <option value="nein" selected disabled>Bitte wählen:</option>
                                                <option value="nein">noch kein Nachweis</option>
                                                <option value="ALGII">ALGII</option>
                                                <option value="ALGI">ALGI</option>
                                                <option value="Asyl">Asyl</option>
                                                <option value="Schüler">Schüler</option>
                                                <option value="Rente">Rentner</option>
                                                <option value="gemeinnützige Org.">gemeinnützige Org.</option>
                                                <option value="Schwerbehindertenausweis">Schwerbehindertenausweis
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Multiple Radios (inline) -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Nachweis_vorhanden">Nachweis
                                            gesehen:</label>
                                        <div class="col-md-4">
                                            <label class="radio-inline" for="Nachweis_vorhanden-0">
                                                <input name="Nachweis_vorhanden" id="Nachweis_vorhanden-0" value="ja"
                                                       type="radio">
                                                Ja
                                            </label>
                                            <label class="radio-inline" for="Nachweis_vorhanden-1">
                                                <input name="Nachweis_vorhanden" id="Nachweis_vorhanden-1" value="nein"
                                                       checked="checked" type="radio">
                                                Nein
                                            </label>
                                            <span class="help-block">Bitte überprüfe hier den jeweiligen Nachweis ob vorhanden und gültig.</span>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="form-group">
                                      <label class="col-md-4 control-label" for="Nachweis">Scan hochladen:</label>
                                      <div class="col-md-4">
                                          <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                                        <input id="Nachweis" name="Nachweis" class="input-file" type="file">
                                        <span class="help-block">Hier kann der eingescannte Nachweis (PDF Datei) hochgeladen werden.</span>
                                      </div>
                                    </div>
                                    -->

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Anrede">Anrede:</label>
                                        <div class="col-md-4">
                                            <select id="Anrede" name="Anrede" class="form-control" required="">
                                                <option value="Herr">Herr</option>
                                                <option value="Frau">Frau</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Vorname">Vorname:</label>
                                        <div class="col-md-4">
                                            <input id="Vorname" name="Vorname" placeholder=""
                                                   class="form-control input-md" required="" type="text">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Nachname">Nachname:</label>
                                        <div class="col-md-4">
                                            <input id="Nachname" name="Nachname" placeholder=""
                                                   class="form-control input-md" required="" type="text">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Anschrift">Anschrift:</label>
                                        <div class="col-md-4">
                                            <input id="Anschrift" name="Anschrift" placeholder="Str. PLZ Ort"
                                                   class="form-control input-md" required="" type="text">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Telefon">Telefon:</label>
                                        <div class="col-md-4">
                                            <input id="Telefon" name="Telefon" placeholder=""
                                                   class="form-control input-md" type="text">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Mobil">Mobil: </label>
                                        <div class="col-md-4">
                                            <input id="Mobil" name="Mobil" placeholder="" class="form-control input-md"
                                                   type="text">
                                            <span class="help-block">Bitte in folgendem Format: +49xxxxxxxxxxx</span>

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Email">Email:</label>
                                        <div class="col-md-4">
                                            <input id="Email" name="Email" placeholder="" class="form-control input-md"
                                                   type="text">

                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Anmerkung">Anmerkungen:</label>
                                        <div class="col-md-4">
                                            <textarea class="form-control" id="Anmerkung" name="Anmerkung"></textarea>
                                        </div>
                                    </div>

                                    <!-- Button (Double) -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Reset"></label>
                                        <div class="col-md-8">
                                            <button id="Reset" name="Reset" class="btn btn-danger">Eingaben
                                                zurücksetzen
                                            </button>
                                            <button id="Submit" type="submit" name="Submit" class="btn btn-primary">
                                                Kunden eintragen
                                            </button>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                            </br>
                            </br>
                            </br>

                            <!-- Bootstrap core JavaScript
                            ================================================== -->
                            <!-- Placed at the end of the document so the pages load faster -->
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                            <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
                            <script src="../../dist/js/bootstrap.min.js"></script>

</body>
</html>
