<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
require '../antrag_db/db_functions.php';

if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}


//Auswertung der Daten aus Formular antrag_form/index.php

date_default_timezone_set('Europe/Berlin');

require_once('../rechner_db/inc/db.php');


//$Rechner_ID = (isset($_POST["Rechner_ID"]) AND is_numeric($_POST["Rechner_ID"]) AND $_POST["Rechner_ID"] > 0 AND $_POST["Rechner_ID"] < 9999 ) ? $_POST ["Rechner_ID"] : "error";


$anzahlNachStatus = new selectDB();

$count_alle = $anzahlNachStatus->countAll();
$ID = $count_alle + 1;


$heute = date("Y-m-d");
$Eingang_Datum =  $heute;


//Prüfung der Eingaben
$Nachweis_vorhanden = (isset($_POST["Nachweis_vorhanden"])) ? htmlspecialchars($_POST["Nachweis_vorhanden"]) : "";

$Via = (isset($_POST["Via"])) ? htmlspecialchars($_POST["Via"]) : "";
$Nachweis_Typ = (isset($_POST["Nachweis_Typ"])) ? htmlspecialchars($_POST["Nachweis_Typ"]) : "";
$Anrede = (isset($_POST["Anrede"])) ? htmlspecialchars($_POST["Anrede"]) : "";
$Vorname = (isset($_POST["Vorname"])) ? htmlspecialchars($_POST["Vorname"]) : "error";
$Nachname = (isset($_POST["Nachname"])) ? htmlspecialchars($_POST["Nachname"]) : "error";
$Anschrift = (isset($_POST["Anschrift"])) ? htmlspecialchars($_POST["Anschrift"]) : "error";
$Telefon = (isset($_POST["Telefon"])) ? htmlspecialchars($_POST["Telefon"]) : "";
$Mobil = (isset($_POST["Mobil"])) ? htmlspecialchars($_POST["Mobil"]) : "";
$Email = (isset($_POST["Email"])) ? htmlspecialchars($_POST["Email"]) : "";
$Anmerkung = (isset($_POST["Anmerkung"])) ? htmlspecialchars($_POST["Anmerkung"]) : "";
$Mitarbeiter = strstr($user['email'], "@", true);

//Prüfung, ob ein Nachweis erbracht wurde.
if ($Nachweis_Typ != "nein" and $Nachweis_vorhanden == "ja") {
    $Nachweis_Am = $heute;
} else {
    $Nachweis_Am = "";
}
//Automatisches setzten des Status je nach Situation
if ($Nachweis_Am == "") {
    $Status = "info fehlt";
}

if ($Nachweis_Am != 0 and $Status != "GESPERRT" and $Status != "verhindert/ will nicht") {
    $Status = "bereit";
}

//Eintragen der Daten somit erstellen eines neuen Datensatzes
mysqli_set_charset($db, "utf8");

if ($Vorname == "error" OR $Nachname == "error" OR $Anschrift == "error") {
    echo '<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><b> Datensatz konnte aufgrund eines Fehlers nicht eingetragen werden. Bitte veruche es erneut, achte ggf. auf die korrekte Schreibweise der ID (1234 statt 01234).</b></div><br>';
}


$insertKunden = new insertDB();

$ergebnis = $insertKunden->insert($ID, $Eingang_Datum, $Via, $Nachweis_Typ, $Nachweis_Am, $Anrede, $Nachname, $Vorname, $Anschrift, $Telefon, $Mobil, $Email, $Status, $Anmerkung, $Mitarbeiter);

//echo $ergebnis;


//Direktumleitung auf die Übersicht der Antrags-db
header("Location: ../antrag_db/index.php");

?>


