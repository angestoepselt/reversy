<?php

session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: ../../index.php");
}

require '../login-page/database.php';
if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);
    $user = NULL;
    if (count($results) > 0) {
        $user = $results;
    }
}
require_once('../rechner_db/inc/db.php');
date_default_timezone_set('Europe/Berlin');
?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../logo-angestoepselt-1petrol_155x155.png">

    <title>reversy</title>


    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/sticky-footer.css" rel="stylesheet">

</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../mainpage/index.php">Angestöpselt - reversy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php //Admins können hier den Anmeldebutton für die Adminoberfläche einsehen.
                if ($_SESSION['admin'] == 1) {
                    echo '<li><a href="../admin/index.php" target="_blank"><b>Admin</b></a></li>';
                }
                ?>
                <li>
                    <a href="../login-page/user.php"><b>Willkommen <?php $extension = ucfirst(strstr($user['email'], "@", true));
                            echo $extension; ?>!</b></a></li>
                <li><a href="#">Hilfe</a></li>
                <li><a href="#">Profil</a></li>
                <li><a href="../login-page/logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="../mainpage/index.php">Übersicht <span class="sr-only">(current)</span></a>
                </li>
                <li><a href="../rechner_db/index.php">Rechner-DB</a></li>
                <li><a href="../antrag_db/index.php">Kunden-DB</a></li>
            </ul>
            <br/>
            <hr/>
            <ul class="nav nav-sidebar">
                <li class="active"><a href="../rechner_db/index.php">Status Mailer</a></li>
            </ul>
        </div>


        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <!-- start "Rechner" form -->
            <form class="form-horizontal" action="" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Status Mail 2.0</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="spenden">Spenden:</label>
                        <div class="col-md-6">
                            <input id="spenden" name="spenden" placeholder="Was wurde gespendet?"
                                   class="form-control input-md" type="text">

                        </div>
                    </div>

                    <!-- Durch dieses Formular können Status-Mails versendet werden.-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="helfer">Helfer:</label>
                        <div class="col-md-6">
                            <input id="helfer" name="helfer"
                                   placeholder="bitte hier alle Helfer mit Komma (und) getrennt eintragen"
                                   class="form-control input-md" required="" type="text">
                            <input type="hidden" name="set" value="ja">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="absenden">Ansender:</label>
                        <div class="col-md-6">
                            <select id="absenden" name="absenden" class="form-control">
                                <option value="" selected disabled>bitte wählen:</option>
                                <option value="Dierk">Dierk</option>
                                <option value="Thomas">Thomas</option>
                                <option value="Moritz">Moritz</option>
                                <option value="Florian">Florian</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="create"></label>
                        <div class="col-md-4">
                            <button id="Submit" name="submit" class="btn btn-primary">Mail erstellen</button>
                        </div>
                    </div>
                    </br></br>

                </fieldset>
            </form>

            <?php
            //Auswertung und Überprüfung der Formulareingaben
            $spenden = "";
            if (isset($_POST['spenden'])) {
                $spenden = trim(htmlspecialchars($_POST['spenden']));
            }
            $helfer = "";
            if (isset($_POST['helfer'])) {
                $helfer = trim(htmlspecialchars($_POST['helfer']));
            }
            $set = "";
            if (isset($_POST['set'])) {
                $set = trim(htmlspecialchars($_POST['set']));
            }
            $absenden = "";
            if (isset($_POST['absenden'])) {
                $absenden = trim(htmlspecialchars($_POST['absenden']));
            }

            $heutemail = date("d.m.Y");
            $heute = date("Y-m-d");

            $temp = $db->query("SELECT COUNT(*) as ausgaben_heute FROM antrag WHERE Ausgabe_Datum = '$heute'") or die($mysqli->error);
            $z = $temp->fetch_array();
            $ausgaben_heute = $z["ausgaben_heute"];


            $temp = $db->query("SELECT COUNT(*) as neu_eingetragen FROM antrag WHERE Eingang_Datum = '$heute'") or die($mysqli->error);
            $z = $temp->fetch_array();
            $neu_eingetragen = $z["neu_eingetragen"];
            ?>
            <form class="form-horizontal" action="" id="statusform" method="post">
                <fieldset>
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="mailtext">Mail:</label>
                        <div class="col-md-6">
                            <input type="hidden" name="send" value="ja">
                            <!-- Zusammengesetzter Email-Text, kann nach erstellen und vor dem Absenden durch den Nutzer nochmals bearbeitet werden. -->
                            <textarea class="form-control" form="statusform" id="mailtext" name="mailtext" rows="10">
Hallo zusammen,

heute waren <?php echo $helfer; ?> da.

                                <?php if ($spenden != "") { ?>
                                    Gespendet wurde: <?php echo $spenden; ?>.<?php } ?>

                                <?php if ($ausgaben_heute != 0) { ?>
                                    Ausgegeben wurden: <?php echo $ausgaben_heute; ?> PC.<?php } ?>

                                <?php if ($neu_eingetragen != 0) { ?>
                                    Es wurden außerdem <?php echo $neu_eingetragen; ?> Neukunden registriert. <?php } ?>


                                Viele Grüße
                                <?php echo $absenden; ?>
					  	   
					    
					    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="create"></label>
                        <div class="col-md-4">
                            <button id="Submit" name="submit" class="btn btn-warning">Mail an Verteiler abschicken
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>


            <?php
            // Das Abschicken der Email erfolgt durch die Erweiterung phpmailer.

            require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

            $mailtext = "";
            if (isset($_POST['mailtext'])) {
                $mailtext = htmlspecialchars(nl2br($_POST['mailtext']));
            }

            $send = "";
            if (isset($_POST['send'])) {
                $send = htmlspecialchars($_POST['send']);
            }

            require('../../config.php');

            $mail = new PHPMailer;

            //$mail->SMTPDebug = 3;                               // Enable verbose debug output
            $mail->setLanguage('de', 'vendor/phpmailer/language/phpmailer.lang-de.php');
            $mail->CharSet = 'UTF-8';

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com;smtp2.example.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $smtpuser;                 // SMTP username
            $mail->Password = $smtppw;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->setFrom('angestoepselt@gmail.com', 'Angestöpselt - reversy');
            $mail->addAddress('team-angestoepselt@googlegroups.com');               // Name is optional


            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Status ' . $heutemail;
            $mail->Body = html_entity_decode($mailtext);

            if ($send == "ja") {
                if (!$mail->send()) {
                    echo 'Email konnte nicht verschickt werden.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    echo 'Email wurde verschickt';
                }
            }
            ?>


        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>

</body>

</html>



