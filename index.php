<?php
//reversy Login-Seite 

//prüfen ob session vorhanden ist
session_start();

if (isset($_SESSION['user_id'])) {
    header("Location: frontend/mainpage/index.php");
}
?>


<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="frontend/logo-angestoepselt-1petrol_155x155.png">

    <title>reversy Login</title>

    <!-- Bootstrap core CSS -->
    <link href="frontend/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="frontend/login-page/signin.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="frontend/css/sticky-footer.css" rel="stylesheet">


</head>
<?php if (!empty($message)): ?>
    <p>
        <?= $message ?>
    </p>
<?php endif; ?>

<script src="TweenLite.min.js"></script>

<body>
<div class="container-fluid">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row">
                        <img src="pic/logo_ausgeschnitten.png" class="img-responsive" alt="Angestöpselt Logo"/>
                    </div>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-signin" name="form1" action="index.php"
                          method="POST">
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>
                            <input name="email" class="form-control" placeholder="Email" type="email">
                            <input name="password" class="form-control" placeholder="Passwort" type="password">
                            <?php

                            //Login mit Überprüfung von Email + Passwort sowie setzten der Seassion variablen
                            require 'frontend/login-page/database.php';

                            if (!empty($_POST['email']) && !empty($_POST['password'])):

                                $records = $conn->prepare('SELECT id,email,password,admin FROM users WHERE email = :email');
                                $records->bindParam(':email', $_POST['email']);
                                $records->execute();
                                $results = $records->fetch(PDO::FETCH_ASSOC);

                                $message = '';

                                if (count($results) > 0 && password_verify($_POST['password'], $results['password'])) {

                                    $_SESSION['user_id'] = $results['id'];
                                    $_SESSION['admin'] = $results['admin'];

                                    //Umleitung auf Mainpage
                                    header("Location: frontend/mainpage/index.php");

                                } else {
                                    echo '<span class="label label-danger" .>' . '<span class="glyphicon glyphicon-alert">' . '</span>' . ' Sorry, Passwort oder Benutzername falsch/nicht vorhanden!' . '</span>';
                                }

                            endif;

                            ?>


                            <br></br>
                            <input name="Submit" class="btn btn-lg btn-success btn-block" type="submit" value="Login »">

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</div>


</html>